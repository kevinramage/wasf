export interface IWarning {
    id?: string;
    title : string;
    risk: string;
    type: string;
    value: string;
    message: string;
    createdAt: string;
    updatedAt: string;
}