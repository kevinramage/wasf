import React from 'react';
import { Tab, Tabs, TextField, Link, Typography } from '@material-ui/core';
import { CSSProperties } from '@material-ui/styles';
import MaterialTable, { Column } from 'material-table';
import { IComponentType } from '../../interfaces/componentType';
import { IWord } from '../../interfaces/word';
import { IControl } from '../../interfaces/control';
import { ComponentTypeService } from '../../service/ComponentType';
import { ControlService } from '../../service/Control';
import { WordService } from '../../service/Word';
import { IResource } from '../../interfaces/resource';
import { ResourceService } from '../../service/Resource';
import { IComment } from '../../interfaces/comment';
import { CommentService } from '../../service/Comment';
import { IEntryPoint } from '../../interfaces/entryPoint';
import { EntryPointService } from '../../service/EntryPoint';

const divBlockStyle : CSSProperties = {
    marginTop: "20px"
};
const linkStyle : CSSProperties = {
    cursor: "pointer"
};
const typographyStyle : CSSProperties = {
    display: "inline-block"
};
const detailPanelStyle : CSSProperties = {
    margin: "20px"
};

const componentTypeColumns : Column<IComponentType>[] = [
    { title: "Main", field: "main", type: "boolean" },
    { title: "Type", field: "type" },
    { title: "Scoring", field: "score", type: "numeric" }
];

const wordColumns : Column<IWord>[] = [
    { title: "Word", field: "word" },
    { title: "Count", field: "counter" }
];

const controlColums : Column<IControl>[] = [
    { title: "Id", field: "id" },
    { title: "Type", field: "type"},
    { title: "Identifier", field: "identifier" },
    { title: "Attributes", field: "attributesCount" }
];

const commentColumns : Column<IComment>[] = [
    { title: "Id", field: "id" },
    { title: "Comment", field: "comment" }  
];

const entryPointColumns : Column<IEntryPoint>[] = [
    { title: "Id", field: "id" },
    { title: "Type", field: "type" },
    { title: "Name", field: "name" }
];

interface IState {
    applicationId: number;
    sessionId: number;
    resourceId: number;
    tabIndex: number;
    resource: IResource,
    componentTypes : IComponentType[],
    controls: IControl[],
    words: IWord[],
    comments : IComment[],
    entryPoints : IEntryPoint[],
    componentTypeLoading: boolean,
    controlLoading: boolean,
    wordLoading: boolean,
    commentLoading: boolean,
    entryPointLoading: boolean
}

interface IProps {
    applicationId: number;
    sessionId: number;
    resourceId: number;
}

export class ResourceView extends React.Component<IProps, IState> {

    private tableRolesRef : any;

    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);
        this.state = {
            applicationId: this.props.applicationId,
            sessionId: this.props.sessionId,
            resourceId: this.props.resourceId,
            tabIndex: 1,
            resource: {
                id: -1,
                identifier: "",
                path: "",
                requestId: -1
            },
            componentTypes: [],
            controls: [],
            words: [],
            comments: [],
            entryPoints: [],
            componentTypeLoading: true,
            controlLoading: true,
            wordLoading: true,
            commentLoading: true,
            entryPointLoading: true
        }
        this.handleChangeTab = this.handleChangeTab.bind(this);
        this.tableRolesRef=React.createRef();
    }

    componentDidMount() {
        const instance = this;
        const { applicationId, sessionId, resourceId } = this.state;

        // Resource
        ResourceService.getResource(applicationId, sessionId, resourceId)
        .then((resource) => {
            instance.setState({ resource: resource });
        }).catch((err) => {
            console.error(err);
        });

        // Component types
        ComponentTypeService.getAllComponentTypes(applicationId, sessionId, resourceId)
        .then((componentTypes) => {
            instance.setState({ componentTypes: componentTypes, componentTypeLoading: false });
        }).catch((err) => {
            console.error(err);
            instance.setState({ componentTypeLoading: false });
        });

        // Controls
        ControlService.getAllControls(applicationId, sessionId, resourceId)
        .then((controls) => {
            instance.setState({ controls: controls, controlLoading: false });
        }).catch((err) => {
            console.error(err);
            instance.setState({ controlLoading: false });
        });

        // Words
        WordService.getAllWords(applicationId, sessionId, resourceId)
        .then((words) => {
            instance.setState({ words: words, wordLoading: false });
        }).catch((err) => {
            console.error(err);
            instance.setState({ wordLoading: false });
        });

        // Comments
        CommentService.getAllComments(applicationId, sessionId, resourceId)
        .then((comments) => {
            instance.setState({ comments: comments, commentLoading: false });
        }).catch((err) => {
            console.error(err);
            instance.setState({ commentLoading: false });
        });

        // Entry points
        EntryPointService.getAllEntryPoints(applicationId, sessionId, resourceId)
        .then((entryPoints) => {
            instance.setState({ entryPoints: entryPoints, entryPointLoading: false });
        }).catch((err) => {
            console.error(err);
            instance.setState({ entryPointLoading: false });
        });
    }

    handleChangeTab(event: React.ChangeEvent<{}>, newValue: number) {
        this.setState({ tabIndex: newValue });
    }

    render() {
        const { tabIndex, resource, componentTypes, controls, words, comments, entryPoints,
            componentTypeLoading, controlLoading, wordLoading, commentLoading, entryPointLoading } = this.state;
        return (
            <div>
                <Tabs value={tabIndex} onChange={this.handleChangeTab}>
                    <Tab label="General" />
                    <Tab label="Type" />
                    <Tab label="Controls" />
                    <Tab label="Words" />
                    <Tab label="Comments" />
                    <Tab label="Entry points" />
                </Tabs>
                { tabIndex === 0 && 
                    <div>
                        <div style={divBlockStyle}>
                            <TextField label="Id" value={resource.id} inputProps={{readOnly: true, disabled: true}}></TextField>
                            <TextField label="Identifier" value={resource.identifier} inputProps={{readOnly: true, disabled: true}}></TextField>
                        </div>
                        <div style={divBlockStyle}>
                            <TextField label="Path" value={resource.path} inputProps={{readOnly: true, disabled: true}} />
                        </div>
                        <div style={divBlockStyle}>
                            <Typography style={typographyStyle}>Get the request linked: </Typography> <Link color="primary" style={linkStyle}>Request {resource.requestId}</Link>
                        </div>
                    </div>
                }
                { tabIndex === 1 &&
                    <div>
                        <MaterialTable title="Component types"
                            columns={componentTypeColumns} data={componentTypes} isLoading={componentTypeLoading}
                            detailPanel= {(rowPanel) => {
                                const lines = rowPanel.detail.split("\n").map(line => {
                                    return (
                                        <Typography>{ line }</Typography>
                                    )
                                });
                                return (
                                    <div style={detailPanelStyle}>{lines}</div>
                                )
                            }}
                            options={{ search: false, paging: false }}
                            />
                    </div>
                }
                { tabIndex === 2 &&
                    <div>
                        <MaterialTable title="Controls"
                            tableRef={this.tableRolesRef} 
                            columns={controlColums} data={controls}
                            isLoading={controlLoading}
                            parentChildData={(row: IControl, rows: IControl[]) : IControl | undefined => {
                                const children = rows.find(r => {
                                    return r.id === row.parentId;
                                });
                                //return children.length > 0 ? children : undefined;
                                return children;
                            } }
                            options={{ search: false, paging: false }}
                        />
                    </div>
                }
                { tabIndex === 3 &&
                    <div>
                        <MaterialTable title="Words"
                            columns={wordColumns} data={words}
                            isLoading={wordLoading}
                            options={{ search: false, paging: false }}
                        />
                    </div>
                }
                { tabIndex === 4 &&
                    <div>
                        <MaterialTable title="Comments"
                            columns={commentColumns} data={comments}
                            isLoading={commentLoading}
                            options={{ search: false, paging: false }}
                        />
                    </div>
                }
                { tabIndex === 5 &&
                    <div>
                        <MaterialTable title="Entry points"
                            columns={entryPointColumns} data={entryPoints}
                            isLoading={entryPointLoading}
                            options={{ search: false, paging: false }}
                            actions={[
                                {
                                    icon: "policy",
                                    tooltip: "Inject on this entry point",
                                    onClick: (event, rowData) => {
                                        const url = "/inject?resId=" + resource.id + "&epId=" + (rowData as IEntryPoint).id
                                        window.location.replace(url)
                                    }
                                }
                            ]}
                        />
                    </div>
                }
            </div>
        )
    }
}