import { ServiceManager } from "../manager/serviceManager";
import { IInjectionType } from "../interfaces/injectionType";

export class InjectionTypeService {

    /**
     * Get all injection types
     */
    static getAllInjectionType() {
        return new Promise<IInjectionType[]>((resolve, reject) => {
            var url = '/injectionType';
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IInjectionType[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}