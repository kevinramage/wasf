import { IRequest } from "./request";

export interface IInjectionPayloadStatus {
    payload?: string;
    result?: string;
    status?: string;
    request ?: IRequest;
}