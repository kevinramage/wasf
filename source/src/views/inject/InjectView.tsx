import React from 'react';
import { Card, InputLabel, TableCell, TableRow, Checkbox, Table, TableBody, Button, TextField, Typography, Dialog } from '@material-ui/core';


import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import ErrorIcon from '@material-ui/icons/Error';


import VisibilityIcon from "@material-ui/icons/Visibility";
import { CSSProperties } from '@material-ui/styles';
import { ResourceService } from '../../service/Resource';
import { IPayload } from '../../interfaces/payload';
import { IInjectionTypeBody } from '../../interfaces/injectionTypeBody';
import { IInjectionPayloadStatus } from '../../interfaces/injectionPayloadStatus';
import { IApplication } from '../../interfaces/application';
import { ISession } from '../../interfaces/session';
import { SessionsComponent } from '../../components/SessionsComponent';
import { ApplicationsComponent } from '../../components/ApplicationsComponent';
import { InjectionConfigurationComponent } from '../../components/InjectionConfigurationComponent';

interface IState {
    applicationId: number;
    sessionId: number;
    resourceId : string;
    payloads: IPayload[];
    globalStatus: string;
    isDialogOpen: boolean;
    selectedPayload: IPayload | null;
    reference: string;
    defaultValue: string;
    difference: string;
}

interface IProps {
    applicationId: number;
    sessionId: number;
    onApplicationChanged: (application : IApplication) => void;
    onSessionChanged: (session : ISession) => void;
}




const cardStyle : CSSProperties = {
    marginLeft: "30px",
    padding: "20px"
};
const tableRow : CSSProperties = {
    height: "40px"
};
const tableCell : CSSProperties = {
    padding: "0px"
};
const payloadSectionStyle : CSSProperties = {
    marginTop: "50px"
};
const payloadStateIconStyle : CSSProperties = {
    fontSize: "xx-large",
    marginRight: "20px"
};
const payloadStateTextStyle : CSSProperties = {
    display: "inline-block",
    verticalAlign: "top",
    marginTop: "8px"
};
const dialogStyle : CSSProperties = {
    padding: "30px"
};
const dialogFormLabel : CSSProperties = {
    width: "100px"
};
const dialogHeadersStyle : CSSProperties = {
    marginTop: "50px"
};





export class InjectView extends React.Component<IProps, IState> {

    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);
        this.state = {
            applicationId: this.props.applicationId,
            sessionId: this.props.sessionId,
            resourceId: "",
            payloads: [],
            globalStatus: "",
            isDialogOpen: false,
            selectedPayload: null,
            reference: "",
            defaultValue: "",
            difference: ""
        };

        this.inject = this.inject.bind(this);
        this.onApplicationChanged = this.onApplicationChanged.bind(this);
        this.onSessionChanged = this.onSessionChanged.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onPayloadChanged = this.onPayloadChanged.bind(this);
        this.onDefaultValueChanged = this.onDefaultValueChanged.bind(this);
        this.onDifferenceChanged = this.onDifferenceChanged.bind(this);
        this.onAllPayloadChecked = this.onAllPayloadChecked.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        
    }

    /**
     * Component updated
     */
    componentDidUpdate() {
        //this.loadResources();
    }

    /**
     * Select a payload
     * @param event event
     * @param checked payload checked value
     * @param payload payload linked to the event
     */
    handlePayloadSelect(event: React.ChangeEvent<HTMLInputElement>, checked: boolean, payload: IPayload) {
        const payloads = this.state.payloads;
        payload.selected = checked;
        this.setState({ payloads: payloads });
    }

    /**
     * Click on inject button
     */
    inject(injectionBody : IInjectionTypeBody) {
        const instance = this;
        if ( this.state.applicationId !== -1 && this.state.sessionId !== -1) {

            // Clean previous execution
            const payloads = this.state.payloads;
            payloads.forEach(p => { p.status = ""; });
            this.setState({ globalStatus: "" });
                
            // Call resource service
            ResourceService.inject(this.state.applicationId, this.state.sessionId, injectionBody)
            .then((injectionStatus) => {
                const payloads = instance.state.payloads;

                if ( injectionStatus.status !== "FAILED") {
                    payloads.forEach(p => {
                        const result = (injectionStatus.payloads as IInjectionPayloadStatus[]).find(pl => { return pl.payload === p.payload });
                        if ( result ) {
                            p.request = result.request;
                            p.status = result.status;
                            p.result = result.result;
                        }
                    });
                }

                if ( injectionStatus.reference && injectionStatus.status) {
                    instance.setState({ 
                        reference: injectionStatus.reference, 
                        globalStatus: injectionStatus.status, 
                        payloads: payloads
                    });
                }
                
            }).catch((err) => {
                console.error("Internal error during the injection: ", err);
            });
        }
    }

    /**
     * Select the application to use
     * @param application application changed
     */
    onApplicationChanged(application: IApplication) {
        this.setState({ applicationId: application.id as number});
        if ( this.props.onApplicationChanged ) {
            this.props.onApplicationChanged(application);
        }
    }

    /**
     * Select the session to use
     * @param session session changed
     */
    onSessionChanged(session: ISession) {
        this.setState({ sessionId: session.id as number});
        if ( this.props.onSessionChanged ) {
            this.props.onSessionChanged(session);
        }
    }

    /**
     * Open dialog window
     */
    openDialog(payload: IPayload) {
        this.setState({ isDialogOpen: true, selectedPayload: payload });
    }

    /**
     * Close dialog window
     */
    closeDialog() {
        this.setState({ isDialogOpen: false });
    }

    /**
     * On payload changed
     * @param payloads payloads changed
     */
    onPayloadChanged(payloads: IPayload[]) {
        this.setState({ payloads: payloads });
    }

    /**
     * On default value changed
     * @param defaultValue default value changed
     */
    onDefaultValueChanged(defaultValue: string) {
        this.setState({ defaultValue: defaultValue });
    }

    /**
     * On difference changed
     * @param difference difference changed
     */
    onDifferenceChanged(difference: string) {
        this.setState({ difference: difference });
    }

    /**
     * All payload checkbox status change
     * @param event event
     * @param checked payload state
     */
    onAllPayloadChecked(event: any, checked: boolean) {
        /*
        const payloads = this.state.payloads;
        payloads.forEach(p => { p.selected = true; });
        this.setState({ payloads: payloads });
        this.forceUpdate();
        */
    }

    /**
     * Render
     */
    render() {
        const { applicationId, sessionId, globalStatus, payloads, defaultValue, difference, isDialogOpen, selectedPayload } = this.state;
        if ( applicationId > 0 && sessionId > 0) {
            return (     
                <Card style={cardStyle}>
                    <InjectionConfigurationComponent applicationId={applicationId} sessionId={sessionId} globalStatus={globalStatus}
                        onInjectQuery={this.inject} onPayloadChanged={this.onPayloadChanged} 
                        onDefaultValueChanged={this.onDefaultValueChanged} onDifferenceChanged={this.onDifferenceChanged} />

                    <div style={payloadSectionStyle}>
                        <InputLabel>Payloads</InputLabel>
                        { /*
                        <Checkbox onChange={this.onAllPayloadChecked}>All payloads</Checkbox>
                        */ }
                        <Table>
                            <TableBody>
                                { payloads.map(p => (
                                    <TableRow key={p.id} style={tableRow}>
                                        <TableCell style={tableCell}>
                                            <Checkbox checked={p.selected} onChange={(e, checked) => {
                                                this.handlePayloadSelect(e, checked, p)
                                            }}
                                            />
                                        </TableCell>
                                        <TableCell>{p.payload}</TableCell>
                                        <TableCell>
                                            { p.status === "INJECTED" && 
                                                <div>
                                                    <ErrorIcon htmlColor="orange" style={payloadStateIconStyle} />
                                                    <Typography style={payloadStateTextStyle} title="Payload worked, check manually if it's not a false positive and fix it if needed">Injected</Typography>
                                                </div>
                                            }
                                            { p.status === "NOT_INJECTED" &&
                                                <div>
                                                    <CheckIcon htmlColor="green" style={payloadStateIconStyle} />
                                                    <Typography style={payloadStateTextStyle} title="Payload not worked for this entry point">Not injected</Typography>
                                                </div>
                                            }
                                            { p.status === "FAILED" &&
                                                <div>
                                                    <ClearIcon htmlColor="red" style={payloadStateIconStyle} />
                                                    <Typography style={payloadStateTextStyle} title="An exeption occured during the injection, contact your administrator for additionnal information">Failed</Typography>
                                                </div>
                                            }
                                        </TableCell>
                                        <TableCell>
                                            { (p.status === "INJECTED" || p.status === "NOT_INJECTED" || p.status === "FAILED") &&
                                                <Button startIcon={<VisibilityIcon />} onClick={() => this.openDialog(p) }>Check</Button>
                                            }
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </div>
                    <Dialog open={isDialogOpen} onClose={this.closeDialog}>
                        <div>
                        { selectedPayload && selectedPayload.request && selectedPayload.request.headers && 
                        <div style={dialogStyle}>      
                            <div>
                                <TextField label="Default value" value={defaultValue} disabled></TextField>
                                <TextField label="Difference" value={difference} disabled></TextField>
                            </div>
                            <div>
                                <div>
                                    <TextField label="Payload" value={selectedPayload.payload} disabled></TextField>
                                    <TextField label="Result" value={selectedPayload.result} disabled style={dialogFormLabel}></TextField>
                                    <TextField label="Status" value={selectedPayload.status} disabled></TextField>
                                </div>
                                <div>
                                    <div>
                                        <TextField label="Protocol" value={selectedPayload.request.protocol} disabled></TextField>
                                        <TextField label="Method" value={selectedPayload.request.method} disabled style={dialogFormLabel}></TextField>
                                        <TextField label="Host" value={selectedPayload.request.host} disabled></TextField>
                                    </div>
                                    <div>
                                        <TextField label="Url" value={selectedPayload.request.url} disabled></TextField>
                                    </div>
                                    <div>
                                        <TextField label="Body" value={selectedPayload.request.body} disabled></TextField>
                                    </div>
                                    <div style={dialogHeadersStyle}>
                                        { selectedPayload.request.headers.map(h =>
                                            <div key={h.key}>
                                                <TextField label="Key" value={ h.key } disabled></TextField>
                                                <TextField label="Value" value={h.value} disabled></TextField>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                        }
                        </div>
                    </Dialog>
                </Card>
            )
        } else if ( applicationId > 0 ) {
            return (
                <div>
                    <Typography variant="h6">Select a session: </Typography>
                    <SessionsComponent applicationId={applicationId} onSessionChanged={this.onSessionChanged} />
                </div>
            )
        } else {
            return (
                <div>
                    <Typography variant="h6">Select an application: </Typography>
                    <ApplicationsComponent onApplicationChanged={this.onApplicationChanged} />
                </div>
            )
        }
    }
}