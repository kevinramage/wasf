export interface ISession {
    id?: number;
    name: string;
    requestNumber: number;
    activated?: boolean;
}