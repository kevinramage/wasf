import React from 'react';
import { SessionService } from '../service/Session';
import { ISession } from '../interfaces/session';
import MaterialTable, { Column } from 'material-table';
import { CSSProperties } from '@material-ui/styles';

const smallCellStyle : CSSProperties = {
    width: "80px",
    maxWidth: "80px"
};

const sessionColumns: Column<ISession>[] = [
    { title: 'Current', field: 'activated', type: 'boolean', cellStyle: smallCellStyle, headerStyle: smallCellStyle },
    { title: 'Name', field: 'name' },
    { title: 'Requests', field: 'requestNumber' }
];


interface IProps {
    applicationId: number;
    onSessionChanged: (session: ISession) => void;
}

interface IState {
    sessions: ISession[],
    isLoading: boolean
}

export class SessionsComponent extends React.Component<IProps, IState> {

    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);
        this.state = { sessions: [], isLoading: false };
        this.onRowClick = this.onRowClick.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        const instance = this;

        // Load all session
        SessionService.getAllSessions(this.props.applicationId)
        .then((sessions) => {
            instance.setState({ sessions: sessions, isLoading: false });
        }).catch((err) => {
            instance.setState({ sessions: [], isLoading: false });
            console.error(err);
        });
    }

    /**
     * Click on session
     * @param event event 
     * @param rowData row data
     * @param toggleDetailPanel toogle detail
     */
    onRowClick (event?: React.MouseEvent, rowData?: ISession, toggleDetailPanel?: (panelIndex?: number) => void) {
        if ( rowData && this.props.onSessionChanged ) {
            this.props.onSessionChanged(rowData);
        }
    }

    /**
     * Render
     */
    render() {
        const { isLoading, sessions } = this.state;
        return (
            <div>
                <MaterialTable title="Sessions" isLoading={isLoading} data={sessions}
                    columns={sessionColumns} onRowClick={this.onRowClick}
                    options = {{ paging: false,  search: false }} 
                />
            </div>
        )
    }
}