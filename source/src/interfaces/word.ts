export interface IWord {
    id: number;
    word: string;
    counter: number;
}