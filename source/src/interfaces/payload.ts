import { IRequest } from "./request";

export interface IPayload {
    id ?: number;
    payload ?: string;
    selected ?: boolean;
    request ?: IRequest;
    status ?: string;
    result ?: string;
}