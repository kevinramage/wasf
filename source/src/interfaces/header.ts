export interface IHeader {
    id : number;
    key : string;
    value : string;
    createdAt: string;
    updatedAt: string;
}