const GET_applications = require("../mock/GET_applications.json");
const GET_application = require("../mock/GET_application.json");
const POST_application = require("../mock/POST_application.json");
const PUT_application = require("../mock/PUT_application.json");
const GET_sessions = require("../mock/GET_sessions.json");
const GET_technologies = require("../mock/GET_technologies.json");
const GET_warnings = require("../mock/GET_warnings.json");
const GET_requests = require("../mock/GET_requests.json");
const GET_request = require("../mock/GET_request.json");
const GET_resources = require("../mock/GET_resources.json");
const POST_startProfiling = require("../mock/POST_startProfiling.json");
const GET_componentTypes = require("../mock/GET_componentTypes.json");
const GET_languages = require("../mock/GET_languages.json");
const GET_words = require("../mock/GET_words.json");
const GET_controls = require("../mock/GET_controls.json");
const GET_resource = require("../mock/GET_resource.json");
const GET_tests = require("../mock/GET_tests.json");


export class MockServiceManager {

    public delay : number = 1000;

    /**
     * Send a get request
     * @param url url 
     */
    public getUrl(url : string) : Promise<string>{
        return new Promise<string>((resolve, reject) => {
            setTimeout(() => {
                const obj = this.mappingGetUrlToData(url);
                resolve(JSON.stringify(obj));
            }, this.delay);
        });
    }

    /**
     * Send a post request
     * @param url url
     * @param body body
     */
    public postUrl(url : string, body: string) : Promise<string>{
        return new Promise<string>((resolve, reject) => {
            setTimeout(() => {
                const obj = this.mappingPostUrlToData(url);
                resolve(JSON.stringify(obj));
            }, this.delay);
        });
    }

    /**
     * Send a put request
     * @param url url
     * @param body body
     */
    public putUrl(url : string, body: string) : Promise<string>{
        return new Promise<string>((resolve, reject) => {
            setTimeout(() => {
                const obj = this.mappingPutUrlToData(url);
                resolve(JSON.stringify(obj));
            }, this.delay);
        });
    }

    /**
     * Send a delete request
     * @param url url 
     */
    public deleteUrl(url : string) : Promise<string>{
        return new Promise<string>((resolve, reject) => {
            setTimeout(() => {
                resolve("");
            }, this.delay);
        });
    }

    private mappingGetUrlToData(url: string) : object {
        if ( url === "/application" ) {
            return GET_applications;
        } else if ( url.match(/\/application\/[0-9]+$/g)) {
            return GET_application;
        } else if ( url.match(/\/application\/[0-9]+\/session$/g)) {
            return GET_sessions;
        } else if ( url.match(/\/application\/[0-9]+\/session\/[0-9]+\/request$/g)) {
            return GET_requests;
        } else if ( url.match(/\/application\/[0-9]+\/session\/[0-9]+\/request\/[0-9]+$/g)) {
            return GET_request;
        } else if ( url.match(/\/application\/[0-9]+\/technology$/g)) {
            return GET_technologies;
        } else if ( url.match(/\/application\/[0-9]+\/warning$/g)) {
            return GET_warnings;
        } else if ( url.match(/\/application\/[0-9]+\/session\/[0-9]+\/resource$/g)) {
            return GET_resources;
        } else if ( url.match(/\/application\/[0-9]+\/session\/[0-9]+\/resource\/[0-9]+$/g)) {
            return GET_resource;
        } else if ( url.match(/\/application\/[0-9]+\/session\/[0-9]+\/resource\/[0-9]+\/componentType$/g)) {
            return GET_componentTypes;
        } else if ( url.match(/\/application\/[0-9]+\/session\/[0-9]+\/resource\/[0-9]+\/language$/g)) {
            return GET_languages;
        } else if ( url.match(/\/application\/[0-9]+\/session\/[0-9]+\/resource\/[0-9]+\/word$/g)) {
            return GET_words;
        } else if ( url.match(/\/application\/[0-9]+\/session\/[0-9]+\/resource\/[0-9]+\/control$/g)) {
            return GET_controls;
        } else if ( url.match(/\/application\/[0-9]+\/test$/g)) {
            return GET_tests;
        } else {
            return {};
        }
    }

    private mappingPostUrlToData(url: string) : object {
        if ( url === "/application" ) {
            return POST_application;
        } else if ( url.match(/\/application\/[0-9]+\/startProfiling$/g)) {
            return POST_startProfiling;
        } else if ( url.match(/\/application\/[0-9]+\/stopProfiling$/g)) {
            return POST_startProfiling;
        } else {
            return {};
        }
    }

    private mappingPutUrlToData(url: string) : object {
        if ( url.match(/\/application\/[0-9]+/g)) {
            return PUT_application;
        } else {
            return {};
        }
    }
}