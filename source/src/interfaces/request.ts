import { IHeader } from "./header";
import { IResponse } from "./response";

export interface IRequest {
    id?: number;
    uuid ?: string;
    protocol?: string;
    host?: string;
    method?: string;
    url?: string;
    body?: string;
    sentDate?: string;
    headers?: IHeader[];
    createdAt?: string;
    updatedAt?: string;
    response?: IResponse;
    code?: number;
    time?: string;
}