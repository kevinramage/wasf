import React from 'react';
import { Typography } from '@material-ui/core';
import { IResource } from '../../interfaces/resource';
import MaterialTable, { Column } from 'material-table';
import { ResourceService } from '../../service/Resource';
import { NavigationManager } from '../../manager/navigationManager';
import { ApplicationsComponent } from '../../components/ApplicationsComponent';
import { IApplication } from "../../interfaces/application";
import { SessionsComponent } from '../../components/SessionsComponent';
import { ISession } from '../../interfaces/session';

const resourceColumns : Column<IResource>[] = [
    { title: 'Identifier', field: 'identifier' },
    { title: 'Type', field: 'type' },
    { title: 'Path', field: 'path' },
    { title: 'Links', field: 'linkNumber' },
    { title: 'Controls', field: 'controlNumber' }
];

interface IState {
    applicationId: number;
    sessionId: number;
    isLoading: boolean,
    resources: IResource[];
}

interface IProps {
    applicationId: number;
    sessionId: number;
    onApplicationChanged: (application : IApplication) => void;
    onSessionChanged: (session : ISession) => void;
}

export class ResourcesView extends React.Component<IProps, IState> {
    
    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);

        // State
        this.state = {
            applicationId: this.props.applicationId,
            sessionId: this.props.sessionId,
            resources: [],
            isLoading: true
        }

        this.onApplicationChanged = this.onApplicationChanged.bind(this);
        this.onSessionChanged = this.onSessionChanged.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        this.loadResources();
    }

    /**
     * Component updated
     */
    componentDidUpdate() {
        this.loadResources();
    }

    /**
     * Load resources
     */
    loadResources() {
        const instance = this;
        const { applicationId, sessionId } = this.state;

        if ( applicationId !== -1 && sessionId !== -1 && this.state.resources.length === 0) {
            ResourceService.getAllResources(applicationId, sessionId)
            .then((resources) => {
                instance.setState({ resources: resources, isLoading: false });
            }).catch((err) => {
                instance.setState({ resources: [], isLoading: false });
                console.error(err);
            });
        }
    }

    /**
     * Select the application to use
     * @param application application changed
     */
    onApplicationChanged(application: IApplication) {
        this.setState({ applicationId: application.id as number});
        if ( this.props.onApplicationChanged ) {
            this.props.onApplicationChanged(application);
        }
    }

    /**
     * Select the session to use
     * @param session session changed
     */
    onSessionChanged(session: ISession) {
        this.setState({ sessionId: session.id as number});
        if ( this.props.onSessionChanged ) {
            this.props.onSessionChanged(session);
        }
    }

    /**
     * Click on resource
     * @param event event
     * @param rowData row data
     * @param toggleDetailPanel toogle detail 
     */
    handleOnClick(event?: React.MouseEvent, rowData?: IResource, toggleDetailPanel?: (panelIndex?: number) => void) {
        if ( rowData && rowData.id ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, this.state.applicationId);
            NavigationManager.addParameterIntValue(NavigationManager.SESSION, this.state.sessionId);
            NavigationManager.addParameterIntValue(NavigationManager.RESOURCE, rowData.id);
            NavigationManager.navigateTo(NavigationManager.RESOURCE_URL);
        }
    }


    /**
     * Render
     */
    render() {
        const { applicationId, sessionId, resources, isLoading } = this.state;
        if ( applicationId > 0 && sessionId > 0) {
            return (
                <MaterialTable title="Resources" isLoading={isLoading}
                            onRowClick={this.handleOnClick}
                            columns={resourceColumns} data={resources}/>
            )
        } else if ( applicationId > 0 ) {
            return (
                <div>
                    <Typography variant="h6">Select a session: </Typography>
                    <SessionsComponent applicationId={applicationId} onSessionChanged={this.onSessionChanged} />
                </div>
            )
        } else {
            return (
                <div>
                    <Typography variant="h6">Select an application: </Typography>
                    <ApplicationsComponent onApplicationChanged={this.onApplicationChanged} />
                </div>
            )
        }
    }
}