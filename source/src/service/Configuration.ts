import { IConfiguration } from "../interfaces/configuration";
import { ServiceManager } from "../manager/serviceManager";

export class ConfigurationService {

    /**
     * Get all configurations
     */
    static getAllConfigurations() {
        return new Promise<IConfiguration[]>((resolve, reject) => {
            var url = '/configuration';
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IConfiguration[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Create a configuration
     * @param configuration configuration
     */
    static createConfiguration(configuration: IConfiguration) {
        return new Promise<IConfiguration>((resolve, reject) => {
            const requestBody = { key: configuration.key, value: configuration.value };
            ServiceManager.instance.post("/configuration", JSON.stringify(requestBody))
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IConfiguration);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Update a configuration
     * @param configuration configuration to update
     */
    static updateConfiguration(configuration: IConfiguration) {
        return new Promise<IConfiguration>((resolve, reject) => {
            const requestBody = { key: configuration.key, value: configuration.value };
            ServiceManager.instance.put("/configuration/" + configuration.id, JSON.stringify(requestBody))
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IConfiguration);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Delete a configuration
     * @param configuration configuration to delete
     */
    static deleteConfiguration(configuration: IConfiguration) {
        return new Promise<null>((resolve, reject) => {
            ServiceManager.instance.delete("/configuration/" + configuration.id)
            .then(() => {
                resolve(null);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}