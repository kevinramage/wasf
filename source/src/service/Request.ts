import { IRequest } from "../interfaces/request";
import { ServiceManager } from "../manager/serviceManager";

export class RequestService {

    static getAllRequests(appId: number, sessionId: number) {
        return new Promise<IRequest[]>((resolve, reject) => {
            var url = '/application/%d/session/%d/request';
            url = url.replace("%d", appId + "").replace("%d", sessionId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IRequest[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static getRequest(appId: number, sessionId: number, requestId: number) {
        return new Promise<IRequest>((resolve, reject) => {
            var url = '/application/%d/session/%d/request/%d';
            url = url.replace("%d", appId + "")
                .replace("%d", sessionId + "")
                .replace("%d", requestId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IRequest);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}