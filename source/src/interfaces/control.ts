import { IControlAttribute } from "./controlAttribute";

export interface IControl {
    id?: number;
    identifier?: string;
    type?: string;
    text?: string;
    html?: string;
    attributes?: IControlAttribute[];
    attributesCount?: number;
    parentId?: number;
    tableData?: any;
}