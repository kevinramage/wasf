import { IRequest } from "../interfaces/request";
import connect from 'socket.io-client';
import { IResponse } from "../interfaces/response";

export class WebSocketManager {
    private static _instance : WebSocketManager;
    private _io : any;
    private _sendRequestHandler : Function | undefined;
    private _sendResponseHandler : Function | undefined;
    private _interceptRequestHandler : Function | undefined;
    
    /**
     * Init
     */
    public init() {
        this._io = connect("http://localhost:7002");
        this._io.on("SendRequest", this.sendRequest.bind(this))
        this._io.on("SendResponse", this.sendResponse.bind(this));
        this._io.on("InterceptRequest", this.interceptRequest.bind(this));
    }

    /**
     * Start recording
     */
    public startRecording() {
        this._io.emit("StartRecord", {});
    }

    /**
     * Stop recording
     */
    public stopRecording() {
        this._io.emit("StopRecord");
    }

    /**
     * Play request
     * @param request request updated
     */
    public playRequest(request: IRequest) {
        this._io.emit("PlayRequest", request);
    }

    /**
     * Send request event
     * @param request request received
     */
    public sendRequest(request: IRequest) {
        if ( this._sendRequestHandler ) {
            this._sendRequestHandler(request);
        }
    }

    /**
     * Send response event
     * @param response response received
     */
    public sendResponse(response: IResponse) {
        if ( this._sendResponseHandler ) {
            this._sendResponseHandler(response);
        }
    }

    /**
     * Intercept request
     * @param request request
     */
    public interceptRequest(request: IRequest) {
        if ( this._interceptRequestHandler ) {
            this._interceptRequestHandler(request);
        }
    }


    /**
     * Set a handler to manage send request event
     */
    public set sendRequestHandler(value: Function) {
        this._sendRequestHandler = value;
    }

    /**
     * Set a handler to manage send response event
     */
    public set sendResponseHandler(value: Function) {
        this._sendResponseHandler = value;
    }

    /**
     * Set a handler to manage intercept request event
     */
    public set interceptRequestHandler(value: Function) {
        this._interceptRequestHandler = value;
    }

    /**
     * Get unique instance
     */
    public static get instance() {
        if ( !WebSocketManager._instance ) {
            WebSocketManager._instance = new WebSocketManager();
        }
        return WebSocketManager._instance;
    }
}