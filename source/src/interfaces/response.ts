import { IHeader } from "./header";
import { IRequest } from "./request";

export interface IResponse {
    id?: number;
    status?: number;
    statusCode ?:number;
    receptionDate?: Date;
    body?: string;
    headers?: IHeader[];
    createdAt?: string;
    updatedAt?: string;
    time?: string;
    request ?: IRequest;
    receivedDate ?: string;
    type ?: string;
}