import { ICheck } from "../interfaces/check";
import { ServiceManager } from "../manager/serviceManager";

export class CheckService {

    /**
     * Get all session checks
     * @param appId Application id
     * @param sesId Session id
     */
    static getAllChecks(appId: number, sesId: number) {
        return new Promise<ICheck[]>((resolve, reject) => {
            const url = ("/application/%d/session/%d/check")
                .replace("%d", appId + "")
                .replace("%d", sesId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as ICheck[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}