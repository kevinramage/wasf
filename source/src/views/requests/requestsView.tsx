import React from 'react';
import { Typography } from '@material-ui/core';
import MaterialTable, { Column } from 'material-table';
import { IRequest } from '../../interfaces/request';
import { CSSProperties } from '@material-ui/styles';
import { RequestService } from '../../service/Request';
import { NavigationManager } from '../../manager/navigationManager';
import { IApplication } from '../../interfaces/application';
import { ISession } from '../../interfaces/session';
import { ApplicationsComponent } from '../../components/ApplicationsComponent';
import { SessionsComponent } from '../../components/SessionsComponent';

const smallCellStyle : CSSProperties = {
    width: "50px",
    maxWidth: "50px"
}

const mediumCellStyle : CSSProperties = {
    width: "80px",
    maxWidth: "80px"
}

const sessionColumns : Column<IRequest>[] = [
    { title: 'Method', field: 'method', cellStyle: smallCellStyle, headerStyle: smallCellStyle },
    { title: 'Url', field: 'url' },
    { title: 'Code', field: 'code', cellStyle: smallCellStyle, headerStyle: smallCellStyle },
    { title: 'Time', field: 'time', cellStyle: mediumCellStyle, headerStyle: mediumCellStyle },
]

interface IState {
    applicationId: number;
    sessionId: number;
    requests: IRequest[];
    isLoading: boolean;
}

interface IProps {
    applicationId: number;
    sessionId: number;
    onApplicationChanged: (application : IApplication) => void;
    onSessionChanged: (session : ISession) => void;
}

export class RequestsView extends React.Component<IProps, IState> {
    
    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);

        // State
        this.state = {
            applicationId: this.props.applicationId,
            sessionId: this.props.sessionId,
            requests: [],
            isLoading: true
        }

        this.onApplicationChanged = this.onApplicationChanged.bind(this);
        this.onSessionChanged = this.onSessionChanged.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        this.loadRequests();
    }

    /**
     * Component updated
     */
    componentDidUpdate() {
        this.loadRequests();
    }

    /**
     * Load session requests
     */
    loadRequests() {
        const instance = this;
        const { applicationId, sessionId } = this.state;

        if ( applicationId !== -1 && sessionId !== -1 && this.state.requests.length === 0 ) {
            RequestService.getAllRequests(applicationId, sessionId)
            .then((requests) => {
                instance.setState({ requests: requests, isLoading: false });
            }).catch((err) => {
                instance.setState({ requests: [], isLoading: false });
                console.error(err);
            });
        }
    }

    /**
     * Click on request
     * @param event event
     * @param rowData row data
     * @param toggleDetailPanel toogle detail 
     */
    handleOnClick(event?: React.MouseEvent, rowData?: IRequest, toggleDetailPanel?: (panelIndex?: number) => void) {
        if ( rowData && rowData.id ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, this.state.applicationId);
            NavigationManager.addParameterIntValue(NavigationManager.SESSION, this.state.sessionId);
            NavigationManager.addParameterIntValue(NavigationManager.REQUEST, rowData.id);
            NavigationManager.navigateTo(NavigationManager.REQUEST_URL);
        }
    }

    /**
     * Select the application to use
     * @param application application changed
     */
    onApplicationChanged(application: IApplication) {
        this.setState({ applicationId: application.id as number});
        if ( this.props.onApplicationChanged ) {
            this.props.onApplicationChanged(application);
        }
    }

    /**
     * Select the session to use
     * @param session session changed
     */
    onSessionChanged(session: ISession) {
        this.setState({ sessionId: session.id as number});
        if ( this.props.onSessionChanged ) {
            this.props.onSessionChanged(session);
        }
    }
    

    /**
     * Render
     */
    render() {
        const { applicationId, sessionId, requests, isLoading } = this.state;
        if ( applicationId > 0 && sessionId > 0 ) {
            return (
                <MaterialTable title="Requests" isLoading={isLoading}
                            onRowClick={this.handleOnClick}
                            columns={sessionColumns} data={requests}/>
            )
        } else if ( applicationId > 0 ) {
            return (
                <div>
                    <Typography variant="h6">Select a session: </Typography>
                    <SessionsComponent applicationId={applicationId} onSessionChanged={this.onSessionChanged} />
                </div>
            )
        } else {
            return (
                <div>
                    <Typography variant="h6">Select an application: </Typography>
                    <ApplicationsComponent onApplicationChanged={this.onApplicationChanged} />
                </div>
            )
        }
    }
}