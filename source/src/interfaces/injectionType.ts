import { IPayload } from "./payload";

export interface IInjectionType {
    id ?: number;
    type ?: string;
    name ?: string;
    payloads ?: IPayload[];
}