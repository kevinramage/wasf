import { IInjectionPayloadStatus } from "./injectionPayloadStatus";

export interface IInjectionStatus {
    reference?: string;
    status?: string;
    payloads?: IInjectionPayloadStatus[];
    defaultValue?: string;
    difference?: string;
}