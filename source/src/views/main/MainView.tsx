import React from 'react';
import CSS from 'csstype';
import { AppBar, Toolbar, Typography, Drawer, ListItem, ListItemIcon, List, ListItemText, Divider, Tooltip } from '@material-ui/core';

import AppsIcon from '@material-ui/icons/Apps';
import AssessmentIcon from '@material-ui/icons/Assessment';
import HttpIcon from '@material-ui/icons/Http';
import WarningIcon from '@material-ui/icons/Warning';
import SecurityIcon  from '@material-ui/icons/Security';
import PolicyIcon  from '@material-ui/icons/Policy';
import SettingsApplicationsIcon  from '@material-ui/icons/SettingsApplications';
import LanguageIcon  from '@material-ui/icons/Language';

import { IApplication } from '../../interfaces/application';
import { ISession } from '../../interfaces/session';

import { ApplicationsView } from '../applications/ApplicationsView';
import { RequestsView } from '../requests/RequestsView';
import { ResourcesView } from '../resources/ResourcesView';
import { TestsView } from '../tests/TestsView';
import { WarningsView } from '../warnings/WarningsView';
import { ApplicationView } from '../application/ApplicationView';
import { NavigationManager } from '../../manager/navigationManager';
import { RequestView } from '../request/RequestView';
import { ResourceView } from '../resource/ResourceView';
import { InjectView } from '../inject/InjectView';
import { ConfigurationView } from '../configuration/ConfigurationView';
import { ProxyView } from '../proxy/ProxyView';


const drawerWidth = 150;
const appBarHeight = 65;

const appBarStyle : CSS.Properties = {
    marginLeft: drawerWidth + "px",
    paddingLeft: '20px',
    width: `calc(100% - ${drawerWidth}px)`,
    height: appBarHeight + "px"
}
const dividerStyle : CSS.Properties = {
    marginTop: appBarHeight + "px"
}
const listItemStyle : CSS.Properties = {
    cursor: "pointer"
}
const paperStyle : CSS.Properties = {
    marginLeft: (drawerWidth + 50) + "px",
    marginTop: "0px",
    padding: "20px",
    height: `calc(100% - ${appBarHeight}px)`,
    minHeight: `calc(100% - ${appBarHeight}px)`
}


const APPLICATIONS = "APPLICATIONS";
const APPLICATION = "APPLICATION";
const REQUESTS = "REQUESTS";
const REQUEST = "REQUEST";
const RESOURCES = "RESOURCES";
const RESOURCE = "RESOURCE";
const INJECTION = "INJECTION";
const WARNINGS = "WARNINGS";
const TESTS = "TESTS";
const CONFIGURATION = "CONFIGURATION";
const PROXY = "PROXY";

export class MainView extends React.Component {

    private currentView : string = "";
    private applicationId : number = -1;
    private sessionId : number = -1;
    private requestId : number = -1;
    private resourceId : number = -1;

    /**
     * Constructor
     * @param props props
     */
    constructor(props : any) {
        super(props);
        this.selectView(window.location.pathname);

        this.clickOnRequestsView = this.clickOnRequestsView.bind(this);
        this.clickOnResourcesView = this.clickOnResourcesView.bind(this);
        this.clickOnInjectView = this.clickOnInjectView.bind(this);
        this.clickOnWarningsView = this.clickOnWarningsView.bind(this);
        this.clickOnTestsView = this.clickOnTestsView.bind(this);
        this.clickOnConfigurationView = this.clickOnConfigurationView.bind(this);
        this.onApplicationChanged = this.onApplicationChanged.bind(this);
        this.onSessionChanged = this.onSessionChanged.bind(this);
    }

    /**
     * Define the view from the url
     * @param url url
     */
    selectView( url: string ) {
        switch ( url ) {

            // Application
            case "":
            case "/":
            case NavigationManager.APPLICATIONS_URL:
                this.currentView = APPLICATIONS;
            break;
            case NavigationManager.APPLICATION_URL:
                this.currentView = APPLICATION;
                this.applicationId = NavigationManager.getParameterIntValue(NavigationManager.APPLICATION) || -1;
            break;

            // Requests
            case NavigationManager.REQUESTS_URL:
                this.applicationId = NavigationManager.getParameterIntValue(NavigationManager.APPLICATION) || -1;
                this.sessionId = NavigationManager.getParameterIntValue(NavigationManager.SESSION) || -1;
                this.currentView = REQUESTS;
            break;
            case NavigationManager.REQUEST_URL:
                this.applicationId = NavigationManager.getParameterIntValue(NavigationManager.APPLICATION) || -1;
                this.sessionId = NavigationManager.getParameterIntValue(NavigationManager.SESSION) || -1;
                this.requestId = NavigationManager.getParameterIntValue(NavigationManager.REQUEST) || -1;
                this.currentView = REQUEST;
            break;

            // Resources
            case NavigationManager.RESOURCES_URL:
                this.applicationId = NavigationManager.getParameterIntValue(NavigationManager.APPLICATION) || -1;
                this.sessionId = NavigationManager.getParameterIntValue(NavigationManager.SESSION) || -1;
                this.currentView = RESOURCES;
            break;
            case NavigationManager.RESOURCE_URL:
                this.applicationId = NavigationManager.getParameterIntValue(NavigationManager.APPLICATION) || -1;
                this.sessionId = NavigationManager.getParameterIntValue(NavigationManager.SESSION) || -1;
                this.resourceId = NavigationManager.getParameterIntValue(NavigationManager.RESOURCE) || -1;
                this.currentView = RESOURCE;
            break;

            // Injection
            case NavigationManager.INJECTION_URL:
                this.applicationId = NavigationManager.getParameterIntValue(NavigationManager.APPLICATION) || -1;
                this.sessionId = NavigationManager.getParameterIntValue(NavigationManager.SESSION) || -1;
                this.currentView = INJECTION;
            break;

            // Warnings
            case NavigationManager.WARNINGS_URL:
                this.applicationId = NavigationManager.getParameterIntValue(NavigationManager.APPLICATION) || -1;
                this.currentView = WARNINGS;
            break;

            // Tests
            case NavigationManager.TESTS_URL:
                this.applicationId = NavigationManager.getParameterIntValue(NavigationManager.APPLICATION) || -1;
                this.currentView = TESTS;
            break;

            // Configuration
            case NavigationManager.CONFIGURATION_URL:
                this.currentView = CONFIGURATION;
            break;

            // Proxy
            case NavigationManager.PROXY_URL:
                this.currentView = PROXY;
            break;
        }
    }

    /**
     * Click on applications link button
     */
    clickOnApplicationsView() : void {
        NavigationManager.navigateTo(NavigationManager.APPLICATIONS_URL);
    }

    /**
     * Click on requests link button
     */
    clickOnRequestsView() : void {
        if ( this.applicationId && this.applicationId !== -1 ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, this.applicationId)
        }
        if ( this.sessionId && this.sessionId !== -1 ) {
            NavigationManager.addParameterIntValue(NavigationManager.SESSION, this.sessionId)
        }
        NavigationManager.navigateTo(NavigationManager.REQUESTS_URL);
    }

    /**
     * Click on resource link button
     */
    clickOnResourcesView() : void {
        if ( this.applicationId && this.applicationId !== -1 ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, this.applicationId)
        }
        if ( this.sessionId && this.sessionId !== -1 ) {
            NavigationManager.addParameterIntValue(NavigationManager.SESSION, this.sessionId)
        }
        NavigationManager.navigateTo(NavigationManager.RESOURCES_URL);
    }

    /**
     * Click on injection link button
     */
    clickOnInjectView() : void {
        if ( this.applicationId && this.applicationId !== -1 ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, this.applicationId)
        }
        if ( this.sessionId && this.sessionId !== -1 ) {
            NavigationManager.addParameterIntValue(NavigationManager.SESSION, this.sessionId)
        }
        NavigationManager.navigateTo(NavigationManager.INJECTION_URL);
    }

    /**
     * Click on warnings link button
     */
    clickOnWarningsView() : void {
        if ( this.applicationId && this.applicationId !== -1 ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, this.applicationId)
        }
        NavigationManager.navigateTo(NavigationManager.WARNINGS_URL);
    }

    /**
     * Click on tests link button
     */
    clickOnTestsView() : void {
        if ( this.applicationId && this.applicationId !== -1 ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, this.applicationId)
        }
        NavigationManager.navigateTo(NavigationManager.TESTS_URL);
    }

    /**
     * Click on configuration link button
     */
    clickOnConfigurationView() : void {
        NavigationManager.navigateTo(NavigationManager.CONFIGURATION_URL);
    }

    /**
     * Click on proxy link button
     */
    clickOnProxyView() : void {
        NavigationManager.navigateTo(NavigationManager.PROXY_URL);
    }

    /**
     * Select the application to use
     * @param application application changed
     */
    onApplicationChanged(application: IApplication) {
        this.applicationId = application.id as number;
    }

    /**
     * Select the session to use
     * @param session session changed
     */
    onSessionChanged(session: ISession) {
        this.sessionId = session.id as number;
    }


    /**
     * Render
     */
    render() {
        return (
            <div>
                <AppBar position="static" style={appBarStyle}>
                    <Toolbar>
                        <Typography variant="h5">Web Application Security</Typography>
                    </Toolbar>
                </AppBar>
                <nav>
                    <Drawer anchor="left" open={true} variant="permanent">
                        <Divider style={dividerStyle} />
                        <List>
                            <Tooltip title="Show applications view">
                                <ListItem style={listItemStyle} onClick={this.clickOnApplicationsView}>
                                    <ListItemIcon><AppsIcon /></ListItemIcon>
                                    <ListItemText primary="Application" />
                                </ListItem>
                            </Tooltip>
                            <Tooltip title="Show requests view">
                                <ListItem style={listItemStyle}  onClick={this.clickOnRequestsView}>
                                    <ListItemIcon><HttpIcon /></ListItemIcon>
                                    <ListItemText primary="Requests" />
                                </ListItem>
                            </Tooltip>
                            <Tooltip title="Show resources view">
                                <ListItem style={listItemStyle}  onClick={this.clickOnResourcesView}>
                                    <ListItemIcon><AssessmentIcon /></ListItemIcon>
                                    <ListItemText primary="Resources" />
                                </ListItem>
                            </Tooltip>
                            <Tooltip title="Inject some payloads">
                                <ListItem style={listItemStyle}  onClick={this.clickOnInjectView}>
                                    <ListItemIcon><PolicyIcon /></ListItemIcon>
                                    <ListItemText primary="Injection" />
                                </ListItem>
                            </Tooltip>
                            <Divider />
                            <Tooltip title="Proxy">
                                <ListItem style={listItemStyle} onClick={this.clickOnProxyView}>
                                    <ListItemIcon><LanguageIcon /></ListItemIcon>
                                    <ListItemText primary="Proxy" />
                                </ListItem>
                            </Tooltip>
                            <Divider />
                            <Tooltip title="Show warnings view">
                                <ListItem style={listItemStyle} onClick={this.clickOnWarningsView}>
                                    <ListItemIcon><WarningIcon /></ListItemIcon>
                                    <ListItemText primary="Warnings" />
                                </ListItem>
                            </Tooltip>
                            <Tooltip title="Show tests view">
                                <ListItem style={listItemStyle}  onClick={this.clickOnTestsView}>
                                    <ListItemIcon><SecurityIcon /></ListItemIcon>
                                    <ListItemText primary="Tests" />
                                </ListItem>
                            </Tooltip>
                            <Divider />
                            <Tooltip title="Show configuration">
                                <ListItem style={listItemStyle}  onClick={this.clickOnConfigurationView}>
                                    <ListItemIcon><SettingsApplicationsIcon /></ListItemIcon>
                                    <ListItemText primary="Configuration" />
                                </ListItem>
                            </Tooltip>
                        </List>
                    </Drawer>
                </nav>

                <div style={paperStyle}>
                    { this.currentView === APPLICATIONS && 
                        <ApplicationsView />
                    }
                    { this.currentView === APPLICATION && 
                        <ApplicationView id={this.applicationId} />
                    }
                    { this.currentView === REQUESTS && 
                        <RequestsView applicationId={this.applicationId} sessionId={this.sessionId} 
                            onApplicationChanged={this.onApplicationChanged} onSessionChanged={this.onSessionChanged} />
                    }
                    { this.currentView === REQUEST && 
                        <RequestView applicationId={this.applicationId} sessionId={this.sessionId} requestId={this.requestId} />
                    }
                    { this.currentView === RESOURCES && 
                        <ResourcesView applicationId={this.applicationId} sessionId={this.sessionId} 
                            onApplicationChanged={this.onApplicationChanged} onSessionChanged={this.onSessionChanged} />
                    }
                    { this.currentView === RESOURCE && 
                        <ResourceView applicationId={this.applicationId} sessionId={this.sessionId} resourceId={this.resourceId} />
                    }
                    { this.currentView === WARNINGS && 
                        <WarningsView applicationId={this.applicationId} onApplicationChanged={this.onApplicationChanged} />
                    }
                    { this.currentView === TESTS && 
                        <TestsView applicationId={this.applicationId} onApplicationChanged={this.onApplicationChanged} />
                    }
                    { this.currentView === INJECTION && 
                        <InjectView applicationId={this.applicationId} sessionId={this.sessionId}
                            onApplicationChanged={this.onApplicationChanged} onSessionChanged={this.onSessionChanged} />
                    }
                    { this.currentView === CONFIGURATION &&
                        <ConfigurationView />
                    }
                    { this.currentView === PROXY && 
                        <ProxyView />
                    }
                </div>
            </div>
        )
    }
}