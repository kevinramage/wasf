import { IWord } from "../interfaces/word";
import { ServiceManager } from "../manager/serviceManager";

export class WordService {
    static getAllWords(appId: number, sesId: number, resId: number) {
        return new Promise<IWord[]>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource/%d/word';
            url = url.replace("%d", appId + "").replace("%d", sesId + "").replace("%d", resId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IWord[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}