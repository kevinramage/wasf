import React from 'react';
import { IApplication } from '../interfaces/application';
import { ApplicationService } from '../service/Application';
import MaterialTable, { Column } from 'material-table';

const columns : Column<IApplication>[] = [
    { title: 'Id', field: 'id', editable: 'never' },
    { title: 'Name', field: 'name' },
    { title: 'Type', field: 'urlType', editable: "never" },
    { title: 'Created', field: 'createdAt', editable: 'never' },
    { title: 'Updated', field: 'updatedAt', editable: 'never' }
]

interface IProps {
    onApplicationChanged: (application : IApplication) => void;
}

interface IState {
    applications : IApplication[],
    isLoading: boolean
}

/**
 * Applications component
 */
export class ApplicationsComponent extends React.Component<IProps, IState> {

    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);
        this.state = { applications: [], isLoading: false };
        this.onRowClick = this.onRowClick.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        const instance = this;

        // Load all applications
        ApplicationService.getAllApplications()
        .then((applications) => {
            instance.setState({ applications: applications, isLoading: false });
        }).catch((err) => {
            instance.setState({ applications: [], isLoading: false });
            console.error(err);
        });
    }

    /**
     * Click on application
     * @param event event 
     * @param rowData row data
     * @param toggleDetailPanel toogle detail
     */
    onRowClick (event?: React.MouseEvent, rowData?: IApplication, toggleDetailPanel?: (panelIndex?: number) => void) {
        if ( rowData && this.props.onApplicationChanged ) {
            this.props.onApplicationChanged(rowData);
        }
    }

    /**
     * Render
     */
    render() {
        const { applications, isLoading } = this.state;
        return (
            <div>
                <MaterialTable title="Applications"
                    columns={columns} data={applications} isLoading={isLoading}
                    onRowClick={this.onRowClick}
                />
            </div>
        )
    }
}