export interface IComponentType {
    id: number;
    type: string;
    main: boolean;
    score: number;
    detail: string;
}