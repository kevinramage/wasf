import { ILanguage } from "../interfaces/language";
import { ServiceManager } from "../manager/serviceManager";

export class LanguageService {
    static getAllLanguages(appId: number, sesId: number, resId: number) {
        return new Promise<ILanguage[]>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource/%d/language';
            url = url.replace("%d", appId + "").replace("%d", sesId + "").replace("%d", resId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as ILanguage[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}