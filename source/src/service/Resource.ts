import { IResource } from "../interfaces/resource";
import { ServiceManager } from "../manager/serviceManager";
import { IInjectionTypeBody } from "../interfaces/injectionTypeBody";
import { IInjectionStatus } from "../interfaces/injectionStatus";

export class ResourceService {
    
    static getAllResources(applicationId : number, sessionId: number) {
        return new Promise<IResource[]>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource';
            url = url.replace("%d", applicationId + "").replace("%d", sessionId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IResource[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static getResource(appId: number, sesId: number, resId: number) {
        return new Promise<IResource>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource/%d';
            url = url.replace("%d", appId + "").replace("%d", sesId + "").replace("%d", resId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IResource);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static inject(applicationId : number, sessionId: number, injectBody : IInjectionTypeBody) {
        return new Promise<IInjectionStatus>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource/%d/inject';
            url = url.replace("%d", applicationId + "").replace("%d", sessionId + "").replace("%d", injectBody.resourceId + "");
            ServiceManager.instance.post(url, JSON.stringify(injectBody))
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IInjectionStatus);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}