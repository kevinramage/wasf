import { IEntryPoint } from "./entryPoint";

export interface IInjectionTypeBody {
    resourceId : number;
    entryPoints : IEntryPoint[];
    injectionTypeId : number;
    payloadsId : number[];
    defaultValue: string;
    difference: string;
}