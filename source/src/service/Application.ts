import { IApplication } from "../interfaces/application";
import { ServiceManager } from "../manager/serviceManager";

export class ApplicationService {
    
    static getAllApplications() {
        return new Promise<IApplication[]>((resolve, reject) => {
            ServiceManager.instance.get("/application")
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IApplication[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static getApplication(id: number) {
        return new Promise<IApplication>((resolve, reject) => {
            ServiceManager.instance.get("/application/" + id)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IApplication);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static createApplication(name: string) {
        return new Promise<IApplication>((resolve, reject) => {
            const requestBody = { name: name };
            ServiceManager.instance.post("/application", JSON.stringify(requestBody))
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IApplication);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static updateApplication(id: number, name: string) {
        return new Promise<IApplication>((resolve, reject) => {
            const requestBody = { name: name };
            ServiceManager.instance.put("/application/" + id, JSON.stringify(requestBody))
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IApplication);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static deleteApplication(id: number) {
        return new Promise<null>((resolve, reject) => {
            ServiceManager.instance.delete("/application/" + id)
            .then(() => {
                resolve(null);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static startProfiling(id: number) {
        return new Promise<null>((resolve, reject) => {
            ServiceManager.instance.post("/application/" + id + "/startProfiling", "")
            .then(() => {
                resolve(null);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    static stopProfiling(id: number) {
        return new Promise<null>((resolve, reject) => {
            ServiceManager.instance.post("/application/" + id + "/stopProfiling", "")
            .then(() => {
                resolve(null);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}