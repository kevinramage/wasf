import { ISession } from "../interfaces/session";
import { ServiceManager } from "../manager/serviceManager";

export class SessionService {
    
    static getAllSessions(appId: number) {
        return new Promise<ISession[]>((resolve, reject) => {
            const url = ("/application/%d/session").replace("%d", appId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as ISession[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}