export class NavigationManager {

    private static COOKIE_NAME = "WAS";

    // Url
    public static APPLICATIONS_URL = "/applications";
    public static APPLICATION_URL = "/application";
    public static REQUESTS_URL = "/requests";
    public static REQUEST_URL = "/request";
    public static RESOURCES_URL = "/resources";
    public static RESOURCE_URL = "/resource";
    public static INJECTION_URL = "/injection";
    public static WARNINGS_URL = "/warnings";
    public static TESTS_URL = "/tests";
    public static CONFIGURATION_URL = "/configuration";
    public static PROXY_URL = "/proxy";


    // Keys
    public static APPLICATION = "AppId";
    public static SESSION = "SesId";
    public static REQUEST = "ReqId";
    public static RESOURCE = "ResId";
    public static REDIR_URL = "Redir";

    private static _context : { [key: string] : string } = {};
    
    /*
    static navigateTo(url : string, redirectionUrl?: string) {
        const _redirectionUrl = NavigationManager.getContextValue(NavigationManager.REDIR_URL);

        // A redirection not defined
        if ( !_redirectionUrl ) {
            if ( redirectionUrl ) {
                NavigationManager.setContextValue(NavigationManager.REDIR_URL, redirectionUrl);
            }
            window.location.replace(url);

        // Redirection defined previously
        } else {
            NavigationManager.resetContextValue(NavigationManager.REDIR_URL);
            window.location.replace(_redirectionUrl);
        }
    }
    */

    static setContextValue(key: string, value: string) {
        const params = this.readCookie();
        params[key] = value;
        this.writeCookie(params);
    }

    static resetContextValue(key: string) {
        const params = this.readCookie();
        const newParams : {[key: string]: string} = {};
        Object.keys(params).forEach(k => {
            if ( k !== key ) {
                newParams[k] = params[k];
            }
        });
        this.writeCookie(newParams);
    }

    static getContextValue(key: string) {
        const params = this.readCookie();
        return params[key];
    }

    static getContextIntValue(key: string) {
        const value = this.getContextValue(key);
        if ( value ) {
            return Number.parseInt(value);
        } else {
            return undefined;
        }
    }

    /**
     * Search a parameter value in url
     * @param key key to search
     */
    static getParameterIntValue(key: string) {
        const urlInfo = new URL(window.location.href);
        const value = urlInfo.searchParams.get(key);
        if ( value ) {
            const ident = Number.parseInt(value);
            if ( !isNaN(ident) ) {
                return ident;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Add integer parameter value in url
     * @param key key to add
     * @param value value associated
     */
    static addParameterIntValue(key: string, value: number) {
        if ( key ) {
            NavigationManager._context[key] = value.toString();
        }
    }

    /**
     * Navigate to a path 
     * @param path path to navigate
     * @param redirectionUrl redirection url
     */
    static navigateTo(path : string, redirectionUrl?: string) {
        if ( path ) {

            // Navigate to url
            const url = NavigationManager.buildUrl(path);
            window.location.replace(url);
        }
    }

    /**
     * Build the final url from path and context
     * @param path path to navigate
     */
    private static buildUrl(path: string) : string {
        var url = path;
        const keys = Object.keys(NavigationManager._context);
        if ( keys.length > 0 ) {
            url += "?" + keys[0] + "=" + NavigationManager._context[keys[0]];
        }
        for ( var i = 1; i < keys.length; i++) {
            url += "&" + keys[i] + "=" + NavigationManager._context[keys[i]];
        }
        return url;
    }

    private static readCookie() {
        const params : {[id: string]: string} = {};
        const cookies = document.cookie.split(";");
        const cookie = cookies.find(c => { return c.split("=")[0] === this.COOKIE_NAME; });
        if ( cookie ) {
            const cookieValue = cookie.split("=")[1];
            cookieValue.split(",").forEach(v => {
                const key = v.split(":")[0];
                const value = v.split(":")[1];
                params[key] = value;
            });
        }
        return params;
    }

    private static writeCookie(params : {[id: string] : string}) {
        var cookieValue = "";
        Object.keys(params).forEach(k => {
            if ( k ) {
                cookieValue += k + ":" + params[k] + ","
            }
        });
        if ( cookieValue !== "" ) {
            cookieValue = cookieValue.substring(0, cookieValue.length - 1);
        }
        document.cookie = this.COOKIE_NAME + "=" + cookieValue;
    }
}