export interface ICheck {
    id?: number;
    type?: string;
    name?: string;
    requestId?: number;
    status?: string;
    statusTitle?: string;
}