import axios from 'axios';

export class HTTPManager {

    private _serverUrl : string;

    constructor() {
        this._serverUrl = "";
    }

    public get(path: string) {
        return new Promise<string>((resolve, reject) => {
            const url = this._serverUrl.concat(path);
            axios.get(url).then((response) => {
                //console.log("Response: ", response.data);
                resolve(JSON.stringify(response.data));
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public post(path: string, body: string) {
        return new Promise<string>((resolve, reject) => {
            const url = this._serverUrl.concat(path);
            axios.post(url, body, {
                headers: {
                    "Content-Type": "application/json"
                }
            }).then((response) => {
                resolve(JSON.stringify(response.data));
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public put(path: string, body: string) {
        return new Promise<string>((resolve, reject) => {
            const url = this._serverUrl.concat(path);
            axios.put(url, body, {
                headers: {
                    "Content-Type": "application/json"
                }
            }).then((response) => {
                resolve(JSON.stringify(response.data));
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public delete(path: string) {
        return new Promise<string>((resolve, reject) => {
            const url = this._serverUrl.concat(path);
            axios.delete(url).then((response) => {
                resolve(JSON.stringify(response.data));
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public get serverUrl() {
        return this._serverUrl;
    }
    public set serverUrl(value: string) {
        this._serverUrl = value;
    }
}