import { IRequest } from "./request";

export interface IRequestExtended extends IRequest {
    code?: number;
    time?: string;
    type?: string;
    responseBody ?: string;
    length ?: string;
    selected?: boolean;
}