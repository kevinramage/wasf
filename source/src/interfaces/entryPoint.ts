
export interface IEntryPoint {
    id ?: number;
    name ?: string;
    type ?: string;
}