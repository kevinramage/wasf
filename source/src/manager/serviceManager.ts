import { MockServiceManager } from "./mockServiceManager";
import { HTTPManager } from "./HTTPManager";

export class ServiceManager {
    private static _instance : ServiceManager;
    private _mockServiceManager : MockServiceManager;
    private _httpManager : HTTPManager;
    private _useMock : boolean;

    /**
     * Constructor
     */
    constructor() {
        this._mockServiceManager = new MockServiceManager();
        this._httpManager = new HTTPManager();
        this._httpManager.serverUrl = "http://localhost:7000/api/v1"
        this._useMock = false;
    }

    /**
     * Send a get request
     * @param url url
     */
    public get(url : string) : Promise<string> {
        if ( this._useMock) {
            return this._mockServiceManager.getUrl(url);
        } else {
            return this._httpManager.get(url);
        }
    }

    /**
     * Send a post request
     * @param url url
     */
    public post(url: string, body: string) : Promise<string> {
        if ( this._useMock ) {
            return this._mockServiceManager.postUrl(url, body);
        } else {
            return this._httpManager.post(url, body);
        }
    }

    /**
     * Send a put request
     * @param url url
     */
    public put(url: string, body: string ) : Promise<string> {
        if ( this._useMock ) {
            return this._mockServiceManager.putUrl(url, body);
        } else {
            return this._httpManager.put(url, body);
        }
    }

    /**
     * Send a delete request
     * @param url url
     */
    public delete(url: string) : Promise<string> {
        if ( this._useMock ) {
            return this._mockServiceManager.deleteUrl(url);
        } else {
            return this._httpManager.delete(url);
        }
    }

    /**
     * Get the unique instance
     */
    static get instance() {
        if ( this._instance == null ) {
            this._instance = new ServiceManager();
        }
        return this._instance;
    }
}