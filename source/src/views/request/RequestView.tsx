import React from 'react';
import { Typography, TextField, Tabs, Tab } from '@material-ui/core';
import { RequestService } from '../../service/Request';
import { IRequest } from '../../interfaces/request';
import { CSSProperties } from '@material-ui/styles';

const blockStyle : CSSProperties = {
    marginTop: "20px"
};
const idStyle : CSSProperties = {
    maxWidth: "60px"
};
const valueStyle : CSSProperties = {
    minWidth: "600px"
}
const bodyStyle : CSSProperties = {
    minWidth: "1000px"
}

interface IState {
    tabIndex: number;
    request?: IRequest | null;
}

interface IProps {
    applicationId: number;
    sessionId: number;
    requestId: number;
}

export class RequestView extends React.Component<IProps, IState> {

    /**
     * Constructor
     * @param props props
     */
    constructor(props: IProps) {
        super(props);
        this.state = {
            tabIndex: 0,
            request: {
                id: 0,
                protocol: "", method: "",
                sentDate: "",
                url: "", body: "", headers: [],
                response: {
                    id: 0,
                    status: 0, time: "",
                    receptionDate: new Date(),
                    body: "", headers: []
                }
            }
        }
        this.handleChangeTab = this.handleChangeTab.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        const instance = this;

        // Request
        RequestService.getRequest(this.props.applicationId, this.props.sessionId, this.props.requestId)
        .then((request) => {
            instance.setState({ request: request });
        }).catch(err => {
            console.error(err);
        });
    }

    /**
     * Change current tab
     * @param event event
     * @param newValue new tab index
     */
    handleChangeTab(event: React.ChangeEvent<{}>, newValue: number) {
        this.setState({ tabIndex: newValue });
    }

    /**
     * Render
     */
    render() {
        const { tabIndex, request } = this.state;
        return (
            <div>
                <Tabs value={tabIndex} onChange={this.handleChangeTab}>
                    <Tab label="Request" />
                    <Tab label="Response" />
                </Tabs>

                { tabIndex === 0 && request && request.headers &&
                <div>
                    <div style={blockStyle}>
                        <TextField label="Id" value={request.id} inputProps={{readOnly: true, disabled: true}}></TextField>
                        <TextField label="Protocol" value={request.protocol} inputProps={{readOnly: true, disabled: true}} ></TextField>
                        <TextField label="Method" value={request.method} inputProps={{readOnly: true, disabled: true}} ></TextField>
                        <TextField label="Sent date" value={request.sentDate} inputProps={{readOnly: true, disabled: true}} ></TextField>
                    </div>
                    <div style={blockStyle}>
                        <TextField label="Url" value={request.url} inputProps={{readOnly: true, disabled: true}} ></TextField>
                    </div>
                    <div style={blockStyle}>
                        <Typography>Headers: </Typography>
                        { request.headers.map(h => {
                            return (
                                <div key={h.id}>
                                    <TextField label="Id" value={h.id} inputProps={{readOnly: true, disabled: true}} style={idStyle}></TextField>
                                    <TextField label="Key" value={h.key} inputProps={{readOnly: true, disabled: true}} ></TextField>
                                    <TextField label="Value" value={h.value} inputProps={{readOnly: true, disabled: true}} style={valueStyle}></TextField>
                                </div>
                            )
                        })}
                    </div>
                    <div>
                        <TextField style={bodyStyle} label="Body" value={request.body} inputProps={{readOnly: true, disabled: true}} multiline={true}></TextField>
                    </div>
                </div>
                }
                { tabIndex === 1 && request && request.response && request.response.headers &&
                    <div>
                        <div style={blockStyle}>
                            <TextField label="Id" value={request.response.id} inputProps={{readOnly: true, disabled: true}} ></TextField>
                            <TextField label="Code" value={request.response.status} inputProps={{readOnly: true, disabled: true}} ></TextField>
                            <TextField label="Time" value={request.response.time} inputProps={{readOnly: true, disabled: true}} ></TextField>
                            <TextField label="Reception date" value={request.response.receptionDate} inputProps={{readOnly: true, disabled: true}} ></TextField>
                        </div>
                        <div style={blockStyle}>
                            <Typography>Headers: </Typography>
                            { request.response.headers.map(h => {
                                return (
                                    <div key={h.id}>
                                        <TextField label="Id" value={h.id} inputProps={{readOnly: true, disabled: true}} style={idStyle}></TextField>
                                        <TextField label="Key" value={h.key} inputProps={{readOnly: true, disabled: true}} ></TextField>
                                        <TextField label="Value" value={h.value} inputProps={{readOnly: true, disabled: true}} style={valueStyle}></TextField>
                                    </div>
                                )
                            })}
                        </div>
                        <div style={blockStyle}>
                            <TextField label="Body"  style={bodyStyle} value={request.response.body} inputProps={{readOnly: true, disabled: true}} multiline={true}></TextField>
                        </div>
                    </div>
                }

            </div>
        )
    }
}