import { IEntryPoint } from "../interfaces/entryPoint";
import { ServiceManager } from "../manager/serviceManager";

export class EntryPointService {

    /**
     * Get all entry points
     * @param appId application id
     * @param sesId session id
     * @param resId resource id
     */
    static getAllEntryPoints(appId: number, sesId: number, resId: number) {
        return new Promise<IEntryPoint[]>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource/%d/entryPoint';
            url = url.replace("%d", appId + "").replace("%d", sesId + "").replace("%d", resId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IEntryPoint[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}