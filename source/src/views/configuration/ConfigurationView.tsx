import React from 'react';
import MaterialTable, { Column } from 'material-table';
import { IConfiguration } from '../../interfaces/configuration';
import { ConfigurationService } from '../../service/Configuration';

const configurationColumns : Column<IConfiguration>[] = [
    { title: "Id", field: "id", editable: "never" },
    { title: "Key", field: "key" },
    { title: "Value", field: "value" }
]

interface IProps {

}
interface IState {
    isLoading: boolean,
    configurations: IConfiguration[]
}

export class ConfigurationView extends React.Component<IProps, IState> {

    /**
     * Constructor
     * @param props props
     */
    constructor(props: IProps) {
        super(props);
        this.state = {
            isLoading: true,
            configurations: []
        }
        this.onRowAdd = this.onRowAdd.bind(this);
        this.onRowUpdate = this.onRowUpdate.bind(this);
        this.onRowDelete = this.onRowDelete.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        const instance = this;

        // Load configurations
        ConfigurationService.getAllConfigurations().then((configurations) => {
            instance.setState({ configurations: configurations, isLoading: false });
        }).catch((err) => {
            console.error("ConfigurationView.componentDidMount - Internal error: ", err);
            instance.setState({ configurations: [], isLoading: false });
        });
    }

    /**
     * Add a row
     * @param configuration configuration
     */
    onRowAdd(configuration: IConfiguration) : Promise<void> {
        const instance = this;
        return new Promise<void>((resolve, reject) => {
            ConfigurationService.createConfiguration(configuration).then((newConfiguration) => {
                const configurations = instance.state.configurations;
                configurations.push(newConfiguration);
                instance.setState({ configurations: configurations });
                resolve();
            }).catch((err) => {
                console.error("An error occured during the configuration creation: ", err);
                reject(err);
            });
        });
    }

    /**
     * Update a row
     * @param newConfiguration new configuration
     * @param oldConfiguration old configuration
     */
    onRowUpdate(newConfiguration: IConfiguration, oldConfiguration: IConfiguration | undefined) : Promise<void> {
        const instance = this;
        return new Promise<void>((resolve, reject) => {
            if ( oldConfiguration ) {
                ConfigurationService.updateConfiguration(newConfiguration).then((configuration) => {
                    const configurations = instance.state.configurations;
                    const index = configurations.findIndex(c => { return c.id === oldConfiguration.id; });
                    if ( index > 0 ) {
                        configurations[index].key = configuration.key;
                        configurations[index].value = configuration.value;
                        instance.setState({ configurations: configurations });
                    }
                    resolve();
                }).catch((err) => {
                    console.error("An error occured during the configuration updation: ", err);
                    reject(err);
                });
            } else {
                resolve();
            }
        });
    }

    /**
     * Delete a row
     * @param oldConfiguration old configuration
     */
    onRowDelete(oldConfiguration: IConfiguration) : Promise<void> {
        const instance = this;
        return new Promise<void>((resolve, reject) => {
            ConfigurationService.deleteConfiguration(oldConfiguration).then(() => {
                const configurations = instance.state.configurations;
                const index = configurations.findIndex(c => { return c.id === oldConfiguration.id; });
                if ( index > -1 ) {
                    configurations.splice(index, 1);
                    instance.setState({ configurations: configurations });
                }
                resolve();
            }).catch((err) => {
                console.error("An error occured during the configuration deletion: ", err);
                reject(err);
            });
        });
    }


    /**
     * Render
     */
    render() {
        const { isLoading, configurations } = this.state;
        return (
            <div>
                <MaterialTable title="Configuration" columns={configurationColumns} options={{
                        pageSize: 10,
                        padding: "dense"
                    }} editable={{
                        onRowAdd: this.onRowAdd,
                        onRowUpdate: this.onRowUpdate,
                        onRowDelete: this.onRowDelete
                    }}
                    isLoading={isLoading} data={configurations} />
            </div>
        )
    }

}