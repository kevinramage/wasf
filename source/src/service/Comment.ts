import { IComment } from "../interfaces/comment";
import { ServiceManager } from "../manager/serviceManager";

export class CommentService {

    /**
     * Get all comments
     * @param appId application id
     * @param sesId session id
     * @param resId resource id
     */
    static getAllComments(appId: number, sesId: number, resId: number) {
        return new Promise<IComment[]>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource/%d/comment';
            url = url.replace("%d", appId + "").replace("%d", sesId + "").replace("%d", resId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IComment[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}