import { IComponentType } from "../interfaces/componentType";
import { ServiceManager } from "../manager/serviceManager";

export class ComponentTypeService {

    /**
     * Get all component types of a resource from its id
     * @param appId application id
     * @param sesId session id
     * @param resId resource id
     */
    static getAllComponentTypes(appId: number, sesId: number, resId: number) {
        return new Promise<IComponentType[]>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource/%d/componentType';
            url = url.replace("%d", appId + "").replace("%d", sesId + "").replace("%d", resId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                const components = object as IComponentType[];
                components.forEach(c => { c.main = false; });
                const sortedComponents = components.sort((a, b) => { return b.score > a.score ? 1: -1; });
                if ( sortedComponents.length > 0) {
                    if ( sortedComponents[0].type === "SEARCH" && sortedComponents.length > 1 && sortedComponents[1].score > 0.75 ) {
                        sortedComponents[1].main = true;
                        const swap = sortedComponents[0];
                        sortedComponents[0] = sortedComponents[1];
                        sortedComponents[1] = swap;
                    } else {
                        sortedComponents[0].main = true;
                    }
                }
                resolve(sortedComponents);
            }).catch((err) => {
                reject(err);
            })
        });
    }
}