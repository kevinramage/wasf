export interface IApplication {
    id?: number;
    name?: string;
    urlType?: string;
    createdAt?: string;
    updatedAt?: string;
}