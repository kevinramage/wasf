export interface ILanguage {
    id: number;
    language: string;
    scoring: string;
}