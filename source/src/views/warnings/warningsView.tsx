import React from 'react';
import { IWarning } from '../../interfaces/warning';
import { WarningService } from '../../service/Warning';
import MaterialTable, { Column } from 'material-table';
import { Typography } from '@material-ui/core';
import { ApplicationsComponent } from '../../components/ApplicationsComponent';
import { IApplication } from '../../interfaces/application';

const warningColumns : Column<IWarning>[] = [
    { title: 'Title', field: 'title' },
    { title: 'Value', field: 'value' },
    { title: 'Message', field: 'message' }
]

interface IState {
    applicationId: number;
    isLoading: boolean,
    warnings: IWarning[];
}

interface IProps {
    applicationId: number;
    onApplicationChanged: (application : IApplication) => void;
}

export class WarningsView extends React.Component<IProps, IState> {
    
    /**
     * Constructor
     * @param props props
     */
    constructor(props : any) {
        super(props);

        // State
        this.state = {
            applicationId: this.props.applicationId,
            warnings: [],
            isLoading: true
        }

        this.onApplicationChanged = this.onApplicationChanged.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        this.loadWarnings();
    }

    /**
     * Component updated
     */
    componentDidUpdate() {
        this.loadWarnings();
    }

    /**
     * Load warnings
     */
    loadWarnings() {
        const instance = this;
        const { applicationId } = this.state;
        if ( applicationId !== -1 && this.state.warnings.length === 0) {
            WarningService.getAllWarnings(applicationId)
            .then((warnings) => {
                instance.setState({ warnings: warnings, isLoading: false });
            }).catch((err) => {
                console.error(err);
            });
        }
    }

    /**
     * Select the application to use
     * @param application application changed
     */
    onApplicationChanged(application: IApplication) {
        this.setState({ applicationId: application.id as number});
        if ( this.props.onApplicationChanged ) {
            this.props.onApplicationChanged(application);
        }
    }

    /**
     * Render
     */
    render() {
        const { applicationId, warnings, isLoading } = this.state;
        if ( applicationId > 0 ) {
            return (
                <div>
                    <MaterialTable title="Warnings" isLoading={isLoading} 
                            columns={warningColumns} data={warnings}/>
                </div>
            )
        } else {
            return (
                <div>
                    <Typography variant="h6">Select an application: </Typography>
                    <ApplicationsComponent onApplicationChanged={this.onApplicationChanged} />
                </div>
            )
        }
    }
}