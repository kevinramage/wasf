import React from 'react';
import { v4 } from 'uuid';
import { ICheck } from '../../interfaces/check';
import { NavigationManager } from '../../manager/navigationManager';
import MaterialTable, { Column } from 'material-table';
import { Link, Typography, Tooltip, Card, CardHeader, CardContent, IconButton, Menu, MenuItem } from '@material-ui/core';
import { CSSProperties } from '@material-ui/styles';
import CheckCircleOutlineIcon  from '@material-ui/icons/CheckCircle';
import HighlightOffOutlinedIcon  from '@material-ui/icons/HighlightOff';
import ScheduleOutlinedIcon  from '@material-ui/icons/Schedule';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { CheckService } from '../../service/Check';
import { IApplication } from '../../interfaces/application';
import { ApplicationsComponent } from '../../components/ApplicationsComponent';

const linkStyle : CSSProperties = {
    cursor: "pointer"
};
const statusStyle : CSSProperties = {
    height: '24px',
    display: 'inline-block',
    verticalAlign: 'top',
    lineHeight: '24px',
    marginLeft: '15px'
};

const clickOnRequest = (id: number) => {
    const url = "/requests/" + id;
    NavigationManager.navigateTo(url);
};

const columns : Column<ICheck>[] = [
    { title: 'Id', field: 'id' },
    { title: 'Type', field: 'type' },
    { title: 'Name', field: 'name' },
    { title: 'Request', field: 'requestId', render:  (data: ICheck, type: ('row' | 'group')) => {
        return (
            <Link color="primary" style={linkStyle} 
            onClick={() => {
                clickOnRequest(data.requestId ? data.requestId : -1)
            }}>Request</Link>
        )
    }},
    { title: 'Status', field: 'status', render: (data: ICheck, type: ('row' | 'group')) => {
        var titleContent;
        if ( data.statusTitle ) {
            titleContent = data.statusTitle.split("\n").map(line => {
                return (
                    <Typography key={v4()}>{line}</Typography>
                )
            });
        }
        switch ( data.status ) {
            case "NoRun":
                return (
                    <div>
                        <ScheduleOutlinedIcon/>
                        <span style={statusStyle}>Passed</span>
                    </div>
                )
            case "Passed":
                return (
                    <Tooltip title={ <div>{titleContent}</div> }>
                        <div>
                            <CheckCircleOutlineIcon stroke="green"/>
                            <span style={statusStyle}>Passed</span>
                        </div>
                    </Tooltip>
                )
            case "Failed":
                return (
                    <Tooltip title={ <div>{titleContent}</div> }>
                        <div>
                            <HighlightOffOutlinedIcon stroke="red"/>
                            <span style={statusStyle}>Failed</span>
                        </div>
                    </Tooltip>
                )
            default:
                return (
                    <Typography>{data.status}</Typography>
                )
        }
    }}
];

interface IState {
    applicationId: number;
    menuAnchorElt?: EventTarget & HTMLButtonElement | null;
    tests : ICheck[];
}

interface IProps {
    applicationId: number;
    onApplicationChanged: (application : IApplication) => void;
}

export class TestsView extends React.Component<IProps, IState> {
    
    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);
        this.state = {
            applicationId: this.props.applicationId,
            menuAnchorElt: null,
            tests: []
        }

        this.handleOpenContextMenu = this.handleOpenContextMenu.bind(this);
        this.handleCloseMenu = this.handleCloseMenu.bind(this);
        this.handleRunTests = this.handleRunTests.bind(this);
        this.onApplicationChanged = this.onApplicationChanged.bind(this);
    }
    
    /**
     * Component loaded
     */
    componentDidMount() {
        this.loadChecks();
    }

    /**
     * Component updated
     */
    componentDidUpdate() {
        this.loadChecks();
    }

    /**
     * Load checks
     */
    loadChecks() {
        const instance = this;
        const { applicationId } = this.state;
        if ( applicationId > 0 && this.state.tests.length === 0) {
            CheckService.getAllChecks(applicationId, 1)
            .then(tests => {
                instance.setState({ tests: tests });
            }).catch((err) => {
                console.error(err);
            });
        }
    }

    /**
     * Open context menu
     * @param e event
     */
    handleOpenContextMenu(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        this.setState({ menuAnchorElt: e.currentTarget });
    }

    /**
     * Close context menu
     */
    handleCloseMenu() {
        this.setState({ menuAnchorElt: null });
    }

    /**
     * Run tests
     */
    handleRunTests() {
        this.handleCloseMenu();
    }

    /**
     * Select the application to use
     * @param application application changed
     */
    onApplicationChanged(application: IApplication) {
        this.setState({ applicationId: application.id as number});
        if ( this.props.onApplicationChanged ) {
            this.props.onApplicationChanged(application);
        }
    }

    /**
     * Render
     */
    render() {
        const { menuAnchorElt, applicationId, tests } = this.state;
        if ( applicationId > 0 ) {
            return (
                <Card>
                    <CardHeader
                        action={
                            <IconButton  onClick={this.handleOpenContextMenu}>
                                <MoreVertIcon />
                            </IconButton>
                        }
                     />
                     <Menu anchorEl={menuAnchorElt} open={Boolean(menuAnchorElt)} onClose={this.handleCloseMenu}>
                         <MenuItem onClick={this.handleRunTests}>Run tests</MenuItem>
                     </Menu>
                    <CardContent>
                        <MaterialTable title="Tests"
                            columns={columns} data={tests}
                        />
                    </CardContent>
                </Card>
            )
        } else {
            return (
                <div>
                    <Typography variant="h6">Select an application: </Typography>
                    <ApplicationsComponent onApplicationChanged={this.onApplicationChanged} />
                </div>
            )
        }
    }
}