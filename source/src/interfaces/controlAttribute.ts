export interface IControlAttribute {
    id: number;
    key: string;
    value: string;
    html: string;
}