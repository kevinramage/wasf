export interface IResource {
    id?: number;
    identifier?: string;
    url?: string;
    completePathname?: string;
    pathname?: string;
    path?: string;
    createdAt?: string;
    updatedAt?: string;
    linkNumber?: number;
    controlNumber?: number;
    type?: string;
    requestId?: number;
}