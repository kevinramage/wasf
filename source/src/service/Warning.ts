import { IWarning } from "../interfaces/warning";
import { ServiceManager } from "../manager/serviceManager";

export class WarningService {

    /**
     * Get all warning of an application
     * @param appId application id
     * @param limit number maximum of warning to get
     */
    static getAllWarnings(appId: number, limit?: number) {
        return new Promise<IWarning[]>((resolve, reject) => {
            var url = ("/application/%d/warning").replace("%d", appId + "");
            if ( limit ) {
                url += "?limit=" + limit;
            }
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as IWarning[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}