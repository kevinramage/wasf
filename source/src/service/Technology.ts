import { ITechnology } from "../interfaces/technology";
import { ServiceManager } from "../manager/serviceManager";

export class TechnologyService {

    static getAllTechnologies(appId: number) {
        return new Promise<ITechnology[]>((resolve, reject) => {
            const url = ("/application/%d/technology").replace("%d", appId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                resolve(object as ITechnology[]);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}