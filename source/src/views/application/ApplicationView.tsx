import React from 'react';
import { Grid, Typography, TextField, Select, MenuItem, InputLabel, Card, CardHeader, IconButton, Menu } from '@material-ui/core';
import { CSSProperties } from '@material-ui/styles';
import MaterialTable, { Column } from 'material-table';
import { ITechnology } from '../../interfaces/technology';
import { IWarning } from '../../interfaces/warning';
import { ISession } from '../../interfaces/session';
import { ApplicationService } from '../../service/Application';
import { IApplication } from '../../interfaces/application';
import { SessionService } from '../../service/Session';
import { TechnologyService } from '../../service/Technology';
import { WarningService } from '../../service/Warning';
import { NavigationManager } from '../../manager/navigationManager';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const technologyColumns : Column<ITechnology>[] = [
    { title: 'Type', field: 'type' },
    { title: 'Value', field: 'value' },
    { title: 'Name', field: 'name' },
    { title: 'Version', field: 'version' }
];

const smallCellStyle : CSSProperties = {
    width: "80px",
    maxWidth: "80px"
}
const warningColumns : Column<IWarning>[] = [
    { title: 'Title', field: 'title' },
    { title: 'Message', field: 'message' },
];

const sessionColumns: Column<ISession>[] = [
    { title: 'Current', field: 'activated', type: 'boolean', cellStyle: smallCellStyle, headerStyle: smallCellStyle },
    { title: 'Name', field: 'name' },
    { title: 'Requests', field: 'requestNumber' }
];

interface IState {
    menuAnchorElt?: EventTarget & HTMLButtonElement | null; 
    application: IApplication;
    applicationId: number;
    applicationName: string;
    sessions: ISession[];
    sessionsLoading: boolean;
    technologies: ITechnology[];
    technologiesLoading: boolean;
    warnings: IWarning[];
    warningsLoading: boolean;
}

interface IProps {
    id: number
}


const gridStyle : CSSProperties = {
    width: "50%",
    minHeight: "200px",
}
const materialTableStyle : CSSProperties = {
    width: "90%"
}
const lineStyle : CSSProperties = {
    marginTop: "20px"
}

const cardStyle : CSSProperties = {
    padding: "10px"
}

export class ApplicationView extends React.Component<IProps, IState> {

    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);
        this.state = {
            menuAnchorElt: null,
            application: { urlType: "UNDEFINED" },
            applicationId: -1, applicationName: "",
            sessions: [], sessionsLoading: true,
            technologies: [], technologiesLoading: true,
            warnings: [], warningsLoading: true
        }

        this.clickOnSession = this.clickOnSession.bind(this);
        this.handleOpenContextMenu = this.handleOpenContextMenu.bind(this);
        this.handleStartProfiling = this.handleStartProfiling.bind(this);
        this.handleStopProfiling = this.handleStopProfiling.bind(this);
        this.handleCloseMenu = this.handleCloseMenu.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        const instance = this;

        // Load application
        ApplicationService.getApplication(this.props.id)
        .then((application) => {
            instance.setState({
                application: application,
                applicationId: application.id || -1,
                applicationName: application.name || ""
            });
        }).catch((err) => {
            console.error(err);
        });

        // Sessions
        this.loadSession();

        // Technologies
        TechnologyService.getAllTechnologies(this.props.id)
        .then((technologies) => {
            instance.setState({ technologies: technologies, technologiesLoading: false });
        }).catch((err) => {
            console.error(err);
        });

        // Warnings
        WarningService.getAllWarnings(this.props.id, 4)
        .then((warnings) => {
            instance.setState({ warnings: warnings, warningsLoading: false });
        }).catch((err) => {
            console.error(err);
        });
    }

    /**
     * Load sessions
     */
    loadSession() {
        const instance = this;
        SessionService.getAllSessions(this.props.id)
        .then((sessions) => {
            instance.setState({ sessions: sessions, sessionsLoading: false });
        }).catch((err) => {
            console.error(err);
        });
    }

    /**
     * Click on session
     * @param event event
     * @param rowData row data
     * @param toggleDetailPanel toggle detail 
     */
    clickOnSession (event?: React.MouseEvent, rowData?: ISession, toggleDetailPanel?: (panelIndex?: number) => void) {
        if ( rowData && rowData.id ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, this.state.applicationId);
            NavigationManager.addParameterIntValue(NavigationManager.SESSION, rowData.id);
            NavigationManager.navigateTo(NavigationManager.RESOURCES_URL);
        }
    }

    /**
     * Open context menu
     * @param e event
     */
    handleOpenContextMenu(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        this.setState({ menuAnchorElt: e.currentTarget });
    }

    /**
     * Click on start profiling
     */
    handleStartProfiling() {
        const instance = this;
        ApplicationService.startProfiling(this.props.id)
        .then(() => {
            instance.loadSession();
            instance.handleCloseMenu();
        }).catch((err) => {
            console.error(err);
        });
    }

    /**
     * Click stop profiling
     */
    handleStopProfiling() {
        const instance = this;
        ApplicationService.stopProfiling(this.props.id)
        .then(() => {
            instance.handleCloseMenu();
        }).catch((err) => {
            console.error(err);
        });
    }

    /**
     * Close context menu
     */
    handleCloseMenu() {
        this.setState({ menuAnchorElt: null });
    }

    /**
     * Render
     */
    render() {
        const { menuAnchorElt, applicationId, sessions, sessionsLoading, application, applicationName,
            technologies, technologiesLoading, warnings, warningsLoading } = this.state;
        return (
            <Card style={cardStyle}>
                <CardHeader 
                    action={
                        <IconButton onClick={this.handleOpenContextMenu}>
                            <MoreVertIcon />
                        </IconButton>
                    }
                />
                <Menu anchorEl={menuAnchorElt} open={Boolean(menuAnchorElt)} onClose={this.handleCloseMenu}>
                    <MenuItem onClick={this.handleStartProfiling}>Start profiling</MenuItem>
                    <MenuItem onClick={this.handleStopProfiling}>Stop profiling</MenuItem>
                </Menu>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Grid container>
                            <Grid item key={0} style={gridStyle}>
                                <Typography variant='h6'>General: </Typography>
                                <div style={lineStyle}>
                                    <TextField label="Id" value={applicationId}  inputProps={{
                                        readOnly: true,
                                        disabled: true,
                                    }}/>
                                    <TextField label="Name" value={applicationName} inputProps={{
                                        readOnly: true,
                                        disabled: true,
                                    }} />
                                </div>
                                <div style={lineStyle}>
                                    <InputLabel htmlFor="type-simple">Type</InputLabel>
                                    <Select value={application.urlType} unselectable="on" inputProps={{
                                        name: 'type',
                                        id: 'type-simple',
                                    }}>
                                        <MenuItem value="UNDEFINED">UNDEFINED</MenuItem>
                                        <MenuItem value="PAGE">PAGE</MenuItem>
                                        <MenuItem value="VIEW">VIEW</MenuItem>
                                    </Select>
                                </div>
                            </Grid>
                            <Grid item key={1} style={gridStyle}>
                                <MaterialTable title="Sessions" isLoading={sessionsLoading} style={materialTableStyle}
                                    columns={sessionColumns} data={sessions}
                                    onRowClick={this.clickOnSession}
                                    options = {{
                                        paging: false,
                                        search: false
                                    }} />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container>
                            <Grid item key={0} style={gridStyle}>
                                <MaterialTable title="Technologies" isLoading={technologiesLoading} style={materialTableStyle}
                                    columns={technologyColumns} data={technologies}
                                    options = {{
                                        paging: false,
                                        search: false
                                    }} />
                            </Grid>
                            <Grid item key={1} style={gridStyle}>
                                <MaterialTable title="Warnings" isLoading={warningsLoading} style={materialTableStyle}
                                    columns={warningColumns} data={warnings}
                                    options = {{
                                        paging: false,
                                        search: false
                                    }} />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Card>
        )
    }
}