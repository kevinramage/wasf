import React from 'react';
import { Card, Button, TableBody, Table, TableCell, TableRow, Tabs, Tab } from '@material-ui/core';
import { CSSProperties } from '@material-ui/styles';

import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
/*import PauseIcon from '@material-ui/icons/Pause';*/
import StopIcon from '@material-ui/icons/Stop';
import { WebSocketManager } from '../../manager/webSocketManager';
import { IRequest } from '../../interfaces/request';
import { IRequestExtended } from '../../interfaces/requestExtended';
import { IResponse } from '../../interfaces/response';


/**
 * ****************************************
 *               STYLE
 * ****************************************
 */

const cardStyle : CSSProperties = {
    marginLeft: "10px",
    marginTop: "5px",
    padding: "10px"
};
const spanBlockStyle : CSSProperties = {
    display: "inline-block",
    marginLeft: "30px"
};
const spanLabelStyle : CSSProperties = {
    fontWeight: "bold"
};
const requestsBlockStyle : CSSProperties = {
    marginLeft: "10px",
    marginTop: "5px",
    padding: "20px",
    height: "250px",
    overflowY: "auto",
};
const lineSelected : CSSProperties = {
    background: "lightblue"
};
const lineUnselected : CSSProperties = {
}
const smallColumn : CSSProperties = {
    width: "80px",
    padding: "4px"
};
const mediumColumn : CSSProperties = {
    width: "120px",
    padding: "4px"
};
const generalRequestForm : CSSProperties = {
    margin: "20px"
};
const requestLabel : CSSProperties = {
    display: "inline-block",
    width: "150px"
};

interface IProps {

}



/**
 * ****************************************
 *               STATE & PROPS
 * ****************************************
 */

interface IState {
    isRecording: boolean;
    indexRequest: number;
    requests: IRequestExtended[];
    playColor: string;
}

export class ProxyView extends React.Component<IProps, IState> {

    private static _counter : number = 0;

    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);
        this.state = {
            isRecording: false,
            indexRequest: 0,
            requests: [],
            playColor: "gray"
        }

        this.handleRecordOnClick = this.handleRecordOnClick.bind(this);
        this.handlePlayOnClick = this.handlePlayOnClick.bind(this);
        this.handlePauseOnClick = this.handlePauseOnClick.bind(this);
        this.handleStopOnClick = this.handleStopOnClick.bind(this);
        this.handleTabOnChange = this.handleTabOnChange.bind(this);
    }

    /**
     * Component loaded
     */
    componentDidMount() {
        WebSocketManager.instance.sendRequestHandler = this.sendRequest.bind(this);
        WebSocketManager.instance.sendResponseHandler = this.sendResponse.bind(this);
        WebSocketManager.instance.init();
    }

    /**
     * When the system send a request
     * @param request request received
     */
    sendRequest(request: IRequest) {
        const requestExtended = request as IRequestExtended;
        const requests = this.state.requests;
        requests.push(requestExtended);
        this.setState({ requests: requests});
    }

    /**
     * when the system receive a response
     * @param response response received
     */
    sendResponse(response: IResponse) {
        const requests = this.state.requests;
        const requestExtended = requests.find(r => { 
            return response.request && r.uuid === response.request.uuid;
        });
        if ( requestExtended && requestExtended.sentDate && response.receivedDate ) {
            const sentDate = new Date(requestExtended.sentDate);
            const receivedDate = new Date(response.receivedDate);
            requestExtended.response = response;
            requestExtended.type = response.type as string;
            requestExtended.code = response.statusCode as number;
            requestExtended.time = this.computeTime(sentDate, receivedDate);
            requestExtended.responseBody = response.body;
            requestExtended.length = response.body ? response.body.length.toString() : "";
            this.setState({ requests: requests });
        }
    }

    /**
     * Compute the response time
     * @param sendDate send date
     * @param receivedDate received date
     */
    private computeTime(sendDate: Date, receivedDate: Date) {
        const diff = receivedDate.getTime() - sendDate.getTime();
        if ( diff > 60000 ) {
            return ( diff / 60000.0 ) + " mn";
        } else if ( diff > 1000 ) {
            return (diff / 1000.0) + " s";
        } else {
            return diff + " ms";
        }
    }



    /**
     * When we received a new request
     * @param request request
     */
    interceptRequestHandler(request: IRequest) {
        //this.setState({ requests: requests, playColor: "green" });
    }

    /**
     * Click on record
     */
    handleRecordOnClick() {
        WebSocketManager.instance.startRecording();
        this.setState({ isRecording: true });
    }

    /**
     * Click on stop
     */
    handleStopOnClick() {
        WebSocketManager.instance.stopRecording();
        this.setState({ isRecording: false });
    }


    /**
     * Click on play
     */
    handlePlayOnClick() {
        WebSocketManager.instance.playRequest({});
        const requests = this.state.requests;
        requests.forEach(r => { r.selected = false; });
        this.setState({ requests: requests, playColor: "gray" });
    }

    /**
     * Click on pause
     */
    handlePauseOnClick() {

    }

    /**
     * Change the tab
     */
    handleTabOnChange(event: any, newValue: number) {
        this.setState({ indexRequest: newValue });
    }

    /**
     * Select a request
     * @param request request
     */
    selectRow(request : IRequestExtended) {
        const requests = this.state.requests;
        requests.forEach(r => { r.selected = false; });
        request.selected = true;
        this.setState({ requests: requests });
    }

    /**
     * Render
     */
    render() {
        const { indexRequest, requests } = this.state;
        const selectedRequest = requests.find(r => { return r.selected });
        return (
            <div>
                <Card style={cardStyle}>
                    <div>
                        { !this.state.isRecording &&
                        <Button><FiberManualRecordIcon htmlColor="red" onClick={this.handleRecordOnClick} /></Button>
                        }
                        { this.state.isRecording &&
                        <Button><StopIcon htmlColor="burlywood" onClick={this.handleStopOnClick} /></Button>
                        }
                        <Button><PlayArrowIcon htmlColor={this.state.playColor} onClick={this.handlePlayOnClick} /></Button>
                        { /*
                        <Button><PauseIcon htmlColor="cadetblue" onClick={this.handlePauseOnClick} /></Button>
                        */ }
                        
                        <span style={spanBlockStyle}><label style={spanLabelStyle}>Context: </label>Request</span>
                        <span style={spanBlockStyle}><label style={spanLabelStyle}>Method: </label>POST</span>
                        <span style={spanBlockStyle}><label style={spanLabelStyle}>Url: </label>/login</span>
                    </div>
                </Card>
                <Card style={requestsBlockStyle}>
                    <Table>
                        <TableBody>
                            { requests.map(r => (
                            <TableRow key={r.uuid} style={r.selected ? lineSelected : lineUnselected }
                                onClick={this.selectRow.bind(this, r)}>
                                <TableCell style={mediumColumn}>{ r.host }</TableCell>
                                <TableCell style={smallColumn}>{ r.method }</TableCell>
                                <TableCell>{ r.url }</TableCell>
                                <TableCell style={smallColumn}>{ r.type }</TableCell>
                                <TableCell style={smallColumn}>{ r.code }</TableCell>
                                <TableCell style={smallColumn}>{ r.time }</TableCell>
                                <TableCell style={smallColumn}>{ r.length }</TableCell>
                            </TableRow>
                            )) }
                        </TableBody>
                    </Table>
                </Card>
                <Card style={cardStyle}>
                    <Tabs value={indexRequest} onChange={this.handleTabOnChange}>
                        <Tab label="General" />
                        <Tab label="Headers" />
                        <Tab label="Response body" />
                        <Tab label="Response headers" />
                    </Tabs>
                    { indexRequest === 0 && selectedRequest && 
                        <div style={generalRequestForm}>
                            <div>
                                <span style={requestLabel}>Host :</span>
                                <span>{ selectedRequest.host }</span>
                            </div>
                            <div>
                                <span style={requestLabel}>Type :</span>
                                <span>{ selectedRequest.type }</span>
                            </div>
                            <div>
                                <span style={requestLabel}>Method :</span>
                                <span>{ selectedRequest.method }</span>
                            </div>
                            <div>
                                <span style={requestLabel}>Url :</span>
                                <span>{ selectedRequest.url }</span>
                            </div>
                            <div>
                                <span style={requestLabel}>Code :</span>
                                <span>{ selectedRequest.code }</span>
                            </div>
                            <div>
                                <span style={requestLabel}>Time :</span>
                                <span>{ selectedRequest.time }</span>
                            </div>
                            <div>
                                <span style={requestLabel}>Body :</span>
                                <span>{ selectedRequest.body }</span>
                            </div>
                            <div>
                                <span style={requestLabel}>Length :</span>
                                <span>{ selectedRequest.length }</span>
                            </div>
                        </div>
                    }
                    { indexRequest === 1 && selectedRequest && selectedRequest.headers && 
                        <div style={generalRequestForm}>
                            { selectedRequest.headers.map(h => (
                                <div key={h.key}>
                                    <span style={requestLabel}>{ h.key }</span>
                                    <span>{ h.value }</span>
                                </div>
                            ))}
                        </div>
                    }
                    { indexRequest === 2 && selectedRequest && selectedRequest.responseBody &&
                        <div style={generalRequestForm}>
                            <span>{ selectedRequest.responseBody }</span>
                        </div>
                    }
                    { indexRequest === 3 && selectedRequest && selectedRequest.response && selectedRequest.response.headers && 
                        <div style={generalRequestForm}>
                            { selectedRequest.response.headers.map(h => (
                                <div key={h.key}>
                                    <span style={requestLabel}>{ h.key }</span>
                                    <span>{ h.value }</span>
                                </div>
                            ))}
                        </div>
                    }
                </Card>
            </div>
        )
    }
}