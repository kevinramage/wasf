import React from 'react';
import MaterialTable, { Column } from 'material-table';
import { IApplication } from '../../interfaces/application';
import { ApplicationService } from '../../service/Application';
import { NavigationManager } from '../../manager/navigationManager';

const columns : Column<IApplication>[] = [
    { title: 'Id', field: 'id', editable: 'never' },
    { title: 'Name', field: 'name' },
    { title: 'Type', field: 'urlType', editable: "never" },
    { title: 'Created', field: 'createdAt', editable: 'never' },
    { title: 'Updated', field: 'updatedAt', editable: 'never' }
]

interface IState {
    applications : IApplication[],
    isLoading: boolean
}
interface IProps {}

export class ApplicationsView extends React.Component<IProps, IState> {

    constructor(props : IProps) {
        super(props);
        this.onRowAdd = this.onRowAdd.bind(this);
        this.onRowUpdate = this.onRowUpdate.bind(this);
        this.onRowDelete = this.onRowDelete.bind(this);
        this.state = {
            applications: [],
            isLoading: true
        }
    }

    componentDidMount() {
        const instance = this;
        ApplicationService.getAllApplications()
        .then((applications) => {
            instance.setState({ applications: applications, isLoading: false });
        }).catch((err) => {
            console.error(err);
        });
    }

    onRowAdd(application: IApplication) : Promise<void> {
        console.debug("ApplicationsView.onRowAdd");
        const instance = this;
        return new Promise<void>((resolve, reject) => {
            ApplicationService.createApplication(application.name || "").then((appCreated) => {
                const apps = instance.state.applications;
                apps.push(appCreated);
                instance.setState({applications: apps});
                resolve();
            }).catch((err) => {
                console.error(err);
                reject(err);
            });
        });
    }

    onRowUpdate(newApplication: IApplication, oldApplication: IApplication | undefined) : Promise<void> {
        console.debug("ApplicationsView.onRowUpdate");
        const instance = this;
        return new Promise<void>((resolve, reject) => {
            if ( oldApplication ) {
                ApplicationService.updateApplication(oldApplication.id || -1, newApplication.name || "").then((appUpdated) => {
                    const apps = instance.state.applications;
                    oldApplication.id = appUpdated.id;
                    oldApplication.name = appUpdated.name;
                    oldApplication.urlType = appUpdated.urlType;
                    oldApplication.createdAt = appUpdated.createdAt;
                    oldApplication.updatedAt = appUpdated.updatedAt;
                    instance.setState({applications: apps});
                    resolve();
                }).catch((err) => {
                    console.error(err);
                    reject(err);
                });
            }
            resolve();
        });
    }

    onRowDelete(oldApplication: IApplication) : Promise<void> {
        console.debug("ApplicationsView.onRowDelete");
        const instance = this;
        return new Promise<void>((resolve, reject) => {
            ApplicationService.deleteApplication(oldApplication.id || -1).then(() => {
                const apps = instance.state.applications;
                const index = apps.indexOf(oldApplication);
                if ( index > -1 ) {
                    apps.splice(index, 1);
                    instance.setState({applications: apps});
                }
                resolve();
            }).catch((err) => {
                console.error(err);
                reject(err);
            });
        });
    }

    onRowClick (event?: React.MouseEvent, rowData?: IApplication, toggleDetailPanel?: (panelIndex?: number) => void) {
        console.debug("ApplicationsView.onRowClick", rowData ? rowData.id :"");
        if ( rowData && rowData.id ) {
            NavigationManager.addParameterIntValue(NavigationManager.APPLICATION, rowData.id);
            NavigationManager.navigateTo(NavigationManager.APPLICATION_URL);
        }
    }


    render() {
        const { applications, isLoading } = this.state;
        return (
            <div>
                <MaterialTable title="Applications"
                    columns={columns} data={applications} isLoading={isLoading}
                    editable={{
                        onRowAdd: this.onRowAdd,
                        onRowUpdate: this.onRowUpdate,
                        onRowDelete: this.onRowDelete
                    }} onRowClick={this.onRowClick}
                />
            </div>
        )
    }
}