import React from 'react';
import { CSSProperties } from '@material-ui/styles';
import { FormControl, InputLabel, Select, MenuItem, Checkbox, Typography, Button, TextField, Dialog } from '@material-ui/core';

import { IResource } from '../interfaces/resource';
import { IPayload } from '../interfaces/payload';
import { IEntryPoint } from '../interfaces/entryPoint';
import { IInjectionType } from '../interfaces/injectionType';

import SearchIcon from '@material-ui/icons/Search';
import FlagIcon from '@material-ui/icons/Flag';
import AssignmentIcon from '@material-ui/icons/Assignment';
import HttpsIcon from '@material-ui/icons/Https';
import PolicyIcon  from '@material-ui/icons/Policy';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import ErrorIcon from '@material-ui/icons/Error';

import { ResourceService } from '../service/Resource';
import { EntryPointService } from '../service/EntryPoint';
import { InjectionTypeService } from '../service/InjectionType';
import { IInjectionTypeBody } from '../interfaces/injectionTypeBody';


/**
 * ****************************************
 *          STYLE
 * ****************************************
 */
const requestStyle : CSSProperties = {
    width: "150px"
}
const entryPointStyle : CSSProperties = {
    width: "150px",
    marginLeft: "30px"
};
const entryPointTextStyle : CSSProperties = {
    display: "inline-block",
    marginLeft: "20px",
    verticalAlign: "top"
};
const entryPointMenuItemStyle : CSSProperties = {
    height: "19px"
};
const entryPointIconStyle : CSSProperties = {
    height: "19px"
};
const injectionTypeStyle : CSSProperties = {
    width: "250px",
    marginLeft: "30px"
};
const injectButtonStyle : CSSProperties = {
    marginLeft: "50px",
    marginTop: "15px",
    verticalAlign: "top"
};
const stateBlockStyle : CSSProperties = {
    display: "inline-block",
    marginLeft: "50px"
};
const stateIconStyle : CSSProperties = {
    fontSize: "xx-large",
    marginTop: "15px"
};
const stateTextStyle : CSSProperties = {
    display: "inline-block",
    verticalAlign: "top",
    marginLeft: "20px",
    marginTop: "20px"
};
const sqlConfigurationStyle : CSSProperties = {
    marginTop: "50px"
};
const entryPointDialogStyle : CSSProperties = {
    width: "500px",
    height: "400px",
    margin: "15px"
};
const entryPointTypeBlockStyle : CSSProperties = {
    display: "block"
};
const entryPointConfigurationStyle : CSSProperties = {
    display: "block",
    marginTop: "100px",
    marginLeft: "200px"
};

/**
 * ****************************************
 *          PROPS AND STATE
 * ****************************************
 */

interface IProps {
    applicationId: number;
    sessionId: number;
    globalStatus: string;
    onInjectQuery: (injectBody: IInjectionTypeBody) => void;
    onPayloadChanged: (payloads : IPayload[]) => void;
    onDefaultValueChanged: (defaultValue: string) => void;
    onDifferenceChanged: (difference: string) => void;
}

interface IState {
    applicationId: number;
    sessionId: number;

    resourceId: string;
    resources: IResource[];

    entryPoints: IEntryPoint[];
    resourceEntryPoints: IEntryPoint[];

    injectionTypeId: string;
    injectionTypes: IInjectionType[];

    payloads: IPayload[];
    defaultValue: string;
    difference: string;
    globalStatus: string;

    isConfigureEntryPointWindow: boolean;
    entryPointType: string;
    entryPointName: string;
}


/**
 * Injection configuration component
 */
export class InjectionConfigurationComponent extends React.Component<IProps, IState> {

    /**
     * Constructor
     * @param props props
     */
    constructor(props : IProps) {
        super(props);
        this.state = {
            applicationId: this.props.applicationId,
            sessionId: this.props.sessionId,
            resourceId: "",
            resources: [],
            entryPoints: [],
            resourceEntryPoints: [],
            injectionTypeId: "",
            injectionTypes: [],
            payloads: [],
            defaultValue: "",
            difference: "50",
            globalStatus: "",
            isConfigureEntryPointWindow: false,
            entryPointType: "QUERY",
            entryPointName: "id"
        }

        this.handleChangeRequest = this.handleChangeRequest.bind(this);
        this.handleChangeEntryPoint = this.handleChangeEntryPoint.bind(this);
        this.handleChangeInjection = this.handleChangeInjection.bind(this);
        this.onChangeDefaultValue = this.onChangeDefaultValue.bind(this);
        this.onChangeDifference = this.onChangeDifference.bind(this);
        this.closeConfigureEntryPointWindow = this.closeConfigureEntryPointWindow.bind(this);
        this.handleEntryPointType = this.handleEntryPointType.bind(this);
        this.handleEntryPointName = this.handleEntryPointName.bind(this);
        this.handleInject = this.handleInject.bind(this);
    }

    /**
     * Load the component
     */
    componentDidMount() {
        this.loadResources();
        this.loadInjectionTypes();
    }

    /**
     * Update the component
     */
    componentDidUpdate(prevProps : IProps ) {
        if ( prevProps.globalStatus !== this.props.globalStatus ) {
            this.setState({ globalStatus: this.props.globalStatus });
        }
    }

    /**
     * Load resources
     */
    loadResources() {
        const instance = this;
        
        // Load resources
        if ( this.state.applicationId !== -1 && this.state.sessionId !== -1 && this.state.resources.length === 0) {
            ResourceService.getAllResources(this.state.applicationId, this.state.sessionId).then((resources) => {
                const state = { resources: resources, resourceId: "" };
                if ( resources.length > 0 && resources[0].id ) {
                    state.resourceId = resources[0].id.toString();
                    instance.changeRequest(state.resourceId);
                }
                instance.setState(state);
            }).catch((err) => {
                console.error("Internal error during the resource loading: ", err);
            });
        }
    }

    /**
     * Load injection types
     */
    loadInjectionTypes() {
        const instance = this;
        InjectionTypeService.getAllInjectionType().then((injectionTypes) => {
            injectionTypes.forEach(e => { (e.payloads as IPayload[]).forEach(p => { p.selected = false })});
            const state = { injectionTypes: injectionTypes, injectionTypeId: "", payloads: new Array<IPayload>() };
            if ( injectionTypes.length > 0 ) {
                state.injectionTypeId = injectionTypes[0].id + ""
                state.payloads = injectionTypes[0].payloads ? injectionTypes[0].payloads : [];
                if ( instance.props.onPayloadChanged ) {
                    instance.props.onPayloadChanged(state.payloads);
                }
            }
            instance.setState(state);
        }).catch((err) => {
            console.error("Internal error during the injection types loading: ", err);
        });
    }

    /**
     * Handle change request
     * @param e event
     */
    handleChangeRequest(e: any) {
        this.changeRequest(e.target.value);
    }

    /**
     * Change the request selected
     * @param resourceId request id
     */
    changeRequest(resourceId : string) {
        const instance = this;
        const id = Number.parseInt(resourceId);
        EntryPointService.getAllEntryPoints(this.state.applicationId, this.state.sessionId, id)
        .then((entryPoints) => {

            const state = { resourceEntryPoints: entryPoints, entryPoints: new Array<IEntryPoint>() };
            if ( entryPoints.length > 0 && entryPoints[0].id ) {
                state.entryPoints.push(entryPoints[0]);
            }
            instance.setState(state);
        }).catch((err) => {
            console.error("Internal error during the resource loading: ", err);
        });
        this.setState({ resourceId: resourceId});
    }

    /**
     * Change entry point
     * @param event event
     */
    handleChangeEntryPoint(event: any) {
        if ( event.target && event.target.value && event.target.value.length > 0) { 
            const entryPoints = this.state.entryPoints;
            const entryPointId = event.target.value[event.target.value.length - 1];
            if ( entryPointId !== -1 ) {
                const entryPointSelected = this.state.resourceEntryPoints.find(e => { return e.id === entryPointId; });
                if ( entryPointSelected ) {
                    const index = entryPoints.indexOf(entryPointSelected);
                    if ( index > -1) {
                        entryPoints.splice(index, 1);
                    } else {
                        entryPoints.push(entryPointSelected);
                    }
                    this.setState({ entryPoints: entryPoints});
                }
            } else {
                this.openConfigureEntryPointWindow();
            }
        }
    }

    /**
     * Change injection type
     * @param e event
     */
    handleChangeInjection(e: any) {
        const state = { injectionTypeId: e.target.value.toString(), payloads: new Array<IPayload>(), difference: "" };
        const injectionTypeId = Number.parseInt(e.target.value);
        const injectionType = this.state.injectionTypes.find(i => { return i.id === injectionTypeId });
        if ( injectionType ) {
            state.payloads = injectionType.payloads ? injectionType.payloads : [];
            switch ( injectionType.type ) {
                case "SQL_BOOLEAN_OR":
                case "SQL_BOOLEAN_AND":
                    state.difference = "50";
                break;
                case "SQL_TIME":
                    state.difference = "100";
                break;
            }
            if ( this.props.onPayloadChanged ) {
                this.props.onPayloadChanged(state.payloads);
            }
        }
        this.setState(state);
    }

    /**
     * Handle inject click
     * @param e event
     */
    handleInject(e: any) {
        const injectionBody = this.generateInjectionTypeBody();
        if ( this.props.onInjectQuery ) {
            this.props.onInjectQuery(injectionBody);
        }
    }

    /**
     * Generate injection service body
     */
    generateInjectionTypeBody() {
        const injectionTypeBody : IInjectionTypeBody = {
            resourceId: Number.parseInt(this.state.resourceId),
            entryPoints: this.state.entryPoints.map(e => {
                if ( e.id ) {
                    return { id: e.id };
                } else {
                    return { type: e.type, name: e.name };
                }
            }),
            injectionTypeId: Number.parseInt(this.state.injectionTypeId),
            payloadsId: this.state.payloads.filter(p => { return p.selected }).map(p => { return p.id as number}),
            defaultValue: this.state.defaultValue,
            difference: this.state.difference
        }
        return injectionTypeBody;
    }

    /**
     * Change the default value
     * @param e event
     */
    onChangeDefaultValue(e: any) {
        this.setState({ defaultValue: e.target.value });
        if ( this.props.onDefaultValueChanged ) {
            this.props.onDefaultValueChanged(e.target.value as string);
        }
    }

    /**
     * Change the difference
     * @param e event
     */
    onChangeDifference(e: any) {
        this.setState({ difference: e.target.value });
        if ( this.props.onDifferenceChanged ) {
            this.props.onDifferenceChanged(e.target.value as string);
        }
    }

    /**
     * Open configure entry point window
     */
    openConfigureEntryPointWindow() {
        this.setState({ isConfigureEntryPointWindow: true });
    }

    /**
     * Close configure entry point window
     */
    closeConfigureEntryPointWindow() {
        this.setState({ isConfigureEntryPointWindow: false });
    }

    /**
     * Handle entry point type
     * @param e event
     */
    handleEntryPointType(e: any) {
        this.setState({ entryPointType: e.target.value });
    }

    /**
     * Handle entry point name
     * @param e event
     */
    handleEntryPointName(e: any) {
        this.setState({ entryPointName: e.target.value });
    }

    /**
     * Render
     */
    render() {
        const { resourceId, resources, entryPoints, resourceEntryPoints, injectionTypeId, injectionTypes,
            globalStatus, defaultValue, difference, isConfigureEntryPointWindow, entryPointType, entryPointName } = this.state;
        return (
            <div>
                <div>
                    { /* Request  */ }
                    <FormControl style={requestStyle}>
                        <InputLabel htmlFor="requestId">Request</InputLabel>
                        <Select value={resourceId} onChange={this.handleChangeRequest}>
                            { resources.map(r => (
                                <MenuItem key={r.id} value={r.id}>{r.identifier}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                    { /* Entry points */ }
                    <FormControl style={entryPointStyle}>
                        <InputLabel htmlFor="entryPoint">Entry Point</InputLabel>
                        <Select value={entryPoints} onChange={this.handleChangeEntryPoint} multiple
                            renderValue={selected => { return (selected as IEntryPoint[]).map(s => { 
                                return s.name}).join(",")}} 
                        >
                            { resourceEntryPoints.map(e => (
                                <MenuItem key={e.id} value={e.id}>
                                    <Checkbox checked={entryPoints.indexOf(e) > -1} />
                                    <div style={entryPointMenuItemStyle}>
                                        { e.type === "QUERY" && <SearchIcon style={entryPointIconStyle} /> }
                                        { e.type === "BODY" &&  <AssignmentIcon style={entryPointIconStyle} /> }
                                        { e.type === "HEADER" && <FlagIcon style={entryPointIconStyle} /> }
                                        { e.type === "COOKIE" && <HttpsIcon style={entryPointIconStyle} />}
                                        <Typography style={entryPointTextStyle}>{e.name}</Typography>
                                    </div>
                                </MenuItem>
                            ))}
                            <MenuItem value="-1">Configure ...</MenuItem>
                        </Select>
                    </FormControl>

                    { /* Injection type */ }
                    <FormControl style={injectionTypeStyle}>
                        <InputLabel htmlFor="injectionType">Injection Type</InputLabel>
                        <Select value={injectionTypeId} onChange={this.handleChangeInjection}>
                            { injectionTypes.map(i => (
                                <MenuItem key={i.id} value={i.id}>{ i.name }</MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                    { /* Inject */ }
                    <Button color="primary" size="small" variant="contained" startIcon={<PolicyIcon />} style={injectButtonStyle}
                            onClick={this.handleInject}>Inject
                    </Button>
                    { globalStatus === "SUCCESS" && 
                        <div style={stateBlockStyle}>
                            <CheckIcon htmlColor="green" style={stateIconStyle} />
                            <Typography style={stateTextStyle} title="All requests sent with success, no secure issue detected for this injection type">Success</Typography>
                        </div>
                    }
                    { globalStatus === "WARNING" && 
                        <div style={stateBlockStyle}>
                            <ErrorIcon htmlColor="orange" style={stateIconStyle} />
                            <Typography style={stateTextStyle} title="Some payloads worked or failed, check payloads below to see injection results">Warning</Typography>
                        </div>
                    }
                    { globalStatus === "FAILED" && 
                        <div style={stateBlockStyle}>
                            <ClearIcon htmlColor="red" style={stateIconStyle} />
                            <Typography style={stateTextStyle} title="An exception occured during the injection execution, contact your administrator for additionnal information">Failed</Typography>
                        </div>
                    }
                </div>
                <div style={sqlConfigurationStyle}>
                    <InputLabel>Configuration</InputLabel>
                    <TextField id="txtDefaultValue" label="Default value" value={defaultValue} onChange={this.onChangeDefaultValue} />
                    <TextField id="txtDiff" label="Difference" value={difference} onChange={this.onChangeDifference} />
                </div>

                <Dialog open={isConfigureEntryPointWindow} onClose={this.closeConfigureEntryPointWindow}>
                    <div style={entryPointDialogStyle}>
                        <FormControl style={entryPointTypeBlockStyle}>
                            <InputLabel htmlFor="entryPointType">Type</InputLabel>
                            <Select value={entryPointType} onChange={this.handleEntryPointType}>
                                <MenuItem value="QUERY">
                                    <SearchIcon/>
                                    <span>Query</span>
                                </MenuItem>
                                <MenuItem value="BODY">
                                    <AssignmentIcon />
                                    <span>Body</span>
                                </MenuItem>
                                <MenuItem value="HEADER">
                                    <FlagIcon/>
                                    <span>Header</span>
                                </MenuItem>
                                <MenuItem value="COOKIE">
                                    <HttpsIcon />
                                    <span>Cookie</span>
                                </MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl>
                            <InputLabel htmlFor="entryPointName"></InputLabel>
                            <TextField id="entryPointName" label="Name" value={entryPointName} onChange={this.handleEntryPointName} />
                        </FormControl>
                        <Button color="primary" variant="contained" style={entryPointConfigurationStyle} onClick={this.closeConfigureEntryPointWindow}>Add</Button>
                    </div>
                </Dialog>
            </div>
        )
    }

}