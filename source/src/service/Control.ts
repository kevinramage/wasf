import { IControl } from "../interfaces/control";
import { ServiceManager } from "../manager/serviceManager";

export class ControlService {

    /**
     * Get all controls for a specific ressource
     * @param appId application id
     * @param sesId session id
     * @param resId ressource id
     */
    static getAllControls(appId: number, sesId: number, resId: number) {
        return new Promise<IControl[]>((resolve, reject) => {
            var url = '/application/%d/session/%d/resource/%d/control';
            url = url.replace("%d", appId + "").replace("%d", sesId + "").replace("%d", resId + "");
            ServiceManager.instance.get(url)
            .then((body) => {
                const object = JSON.parse(body);
                const controls = object as IControl[];
                controls.forEach(c => {
                    c.attributesCount = c.attributes ? c.attributes.length : 0;
                });
                resolve(controls);
            }).catch((err) => {
                reject(err);
            });
        });
    }
}